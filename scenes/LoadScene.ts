import intrude from '../setup/intrude'
import { ATLAS_1, TITLE_ATLAS, BACK_SCENE, BOOT_SCENE, GAME_SCENE, LOAD_SCENE, MENU_SCENE, MODAL_ATLAS, MODAL_SCENE } from '../setup/strings'

export default class LoadScene extends Phaser.Scene {

  constructor() {
    super(LOAD_SCENE)
  }

  preload() {

    let load = this.load
    this.add.image(640, 528, TITLE_ATLAS, 'loadBase')
    let loadBar = this.add.image(520, 528, TITLE_ATLAS, 'loadBar').setScale(0, 1).setOrigin(0, .5)
    let displayVal = 0
    load.on('progress', function (val: number) {
      displayVal += (val - displayVal) / 60
      loadBar.setScale(displayVal, 1)
    })

    load.setPath('assets/images/')
    load.atlas(ATLAS_1, 'qg1.png', 'qg1.json')
    load.atlas(MODAL_ATLAS, 'qg3.png', 'qg3.json')
    load.xml('level2', 'level2.xml')
    load.xml('percent2', 'percent2.xml')

  }

  create() {
    const sc = this
    sc.input.enabled = false

    intrude(sc, [
      'grid_a0',
      'grid_b0',
      'grid_c0',
      'grid_d0',
      'grid_e0',
      'back_a0',
      'back_b0',
      'back_c0',
      'back_d0',
      'back_e0'
    ])

    sc.scene.remove(BOOT_SCENE)
    sc.scene.launch(BACK_SCENE)
    sc.scene.start(GAME_SCENE)
    sc.scene.start(MENU_SCENE)
    sc.scene.start(MODAL_SCENE)
  }

}