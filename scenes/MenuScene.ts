import enter from "../tweens/enter"
import levels from "../setup/levels"
import { GAME_HEIGHT, GAME_WIDTH } from "../setup/const"
import Button from '../ui elements/Button'
import ToggleButton from '../ui elements/ToggleButton'
import intrude from '../setup/intrude'
import { ATLAS_1, BACK_SCENE, GAME_MODE, GAME_SCENE, MENU_SCENE, SETUP_LEVEL, STATE_MODE } from '../setup/strings'
import SceneC from './SceneC'
import RestartModal from "./modals/createRestartModal"
import createAboutModal from "./modals/createAboutModal"
import createRestartModal from "./modals/createRestartModal"
import createHelpModal from "./modals/createHelpModal"

export default class MenuScene extends SceneC {

  public levelComplete: (n: number) => void

  public setCurLevelTo: (n: number) => void

  constructor() {
    super(MENU_SCENE)
  }

  create() {

    const sc = this
    sc.initComponents()

    let levelButtonBases: Button[] = []
    let stars: Phaser.GameObjects.Image[] = []
    let locks: Phaser.GameObjects.Image[] = []

    this.levelComplete = (n) => {
      locks[n - 1].visible = false
      enter(this, stars[n - 1], 100)
      if (n < levels.length - 1) {
        locks[n].visible = false
        enableButton(n)
      }
    }

    const enableButton = (n: number) => {
      if (!levelButtonBases[n].isEnabled) {
        levelButtonBases[n].enableBtn()
        levelButtonBases[n].on('pointerup', function () {
          this.resetBtn()
          let lev = levelButtonBases.indexOf(this)
          sc.scene.sleep(MENU_SCENE)
          sc.inputControl.setInputScene([GAME_SCENE, BACK_SCENE])
          if (sc.levelMgr.getCurLevel() === lev + 1 && lev !== 0) {
            return
          } else {
            sc.levelMgr.setCurLevel(lev + 1)
            let gameScene = sc.scene.get(GAME_SCENE)
            gameScene.events.emit(SETUP_LEVEL)
          }
        })
      }
    }

    intrude(sc, ['levbase'])
    sc.add.image(0, 0, ATLAS_1, 'levbase').setOrigin(0).setDisplaySize(1280, 720).setAlpha(.8)
    sc.add.image(640, 48, ATLAS_1, 'title_sm')

    // calculate level button positions
    let frame = sc.textures.getFrame(ATLAS_1, 'levBtn_u')
    let size = frame.width
    let halfSize = Math.round(size / 2)
    let yAdjust = -10

    // NOTE hard coded for now
    let numCols = 8
    let numRows = 4
    let vGap = 12
    let hGap = 18

    let startX = Math.round((GAME_WIDTH - (numCols * size) - (hGap * (numCols - 1))) / 2)
    let startY = Math.round((GAME_HEIGHT - (numRows * size) - (vGap * (numRows - 1))) / 2) + yAdjust
    let lastLevel = levels.length - 1
    let count = 0


    // add all level buttons
    for (let row = 0; row < numRows; row++) {
      let thisY = startY + row * size + row * vGap
      for (let col = 0; col < numCols; col++) {
        let thisX = startX + col * size + col * hGap
        levelButtonBases[count] = new Button(this, thisX, thisY, ATLAS_1, 'levBtn')
        levelButtonBases[count].setOrigin(0)

        sc.add.bitmapText(thisX + halfSize, thisY + size - 6, 'level', (count + 1).toString()).setOrigin(.5, 1)
        stars[count] = sc.add.image(thisX + halfSize, thisY + halfSize - 19, ATLAS_1, 'star').setScale(0)
        locks[count] = sc.add.image(thisX + halfSize + 30, thisY + halfSize - 28, ATLAS_1, 'lock')

        count++
        if (count === lastLevel) {
          break
        }
      }
      if (count === lastLevel) {
        break
      }
    }


    sc.setCurLevelTo = (n) => {
      for (let i = 0; i < lastLevel; i++) {
        stars[i].setScale(0)
        locks[i].visible = true
        levelButtonBases[i].disableBtn()
        if (i < n - 1) {
          stars[i].setScale(1)
        }
        if (i < n) {
          locks[i].visible = false
          enableButton(i)
        }
      }
    }


    sc.setCurLevelTo(sc.levelMgr.getCurLevel())

    let musicBtn = new ToggleButton(this, 440, 664, ATLAS_1, 'music')
    let soundBtn = new ToggleButton(this, 540, 664, ATLAS_1, 'sound')

    let helpBtn = new Button(this, 640, 664, ATLAS_1, 'help')
    helpBtn.enableBtn()
    helpBtn.on('pointerup', function () {
      createHelpModal(sc)
    })

    let restartBtn = new Button(this, 740, 664, ATLAS_1, 'restart')
    restartBtn.enableBtn()
    restartBtn.on('pointerup', function () {
      createRestartModal(sc)
    })

    let aboutBtn = new Button(this, 840, 664, ATLAS_1, 'about')
    aboutBtn.enableBtn()
    aboutBtn.on('pointerup', function () {
      createAboutModal(sc)
    })

    sc.scene.sleep(MENU_SCENE)

  }
}