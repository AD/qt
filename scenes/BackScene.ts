import LevBtn from '../ui elements/LevBtn'
import { ATLAS_1, BACK_SCENE, GAME_SCENE, SET_BACK } from '../setup/strings'
import GameScene from "./GameScene"
import { GAME_HEIGHT, GAME_WIDTH } from '../setup/const'

export default class BackScene extends Phaser.Scene {

  constructor() {
    super(BACK_SCENE)
  }

  create() {

    let back = this.add.image(0, 0, ATLAS_1, 'back_a0').setOrigin(0)
    back.displayWidth = GAME_WIDTH

    let gameScene = this.scene.get(GAME_SCENE) as GameScene

    new LevBtn(this, gameScene)

    gameScene.events.on(SET_BACK, function (frame: string) {
      back.setFrame(frame)
    })

  }

}