import LevelMgr from '../game components/LevelMgr'
import Detection from '../game components/Detection'
import Grad from '../game components/Grad'
import Grid from '../game components/Grid'
import InputControl from '../game components/InputControl'
import Paths from '../game components/Paths'
import GameState from '../game components/GameState'
import GameC from '../GameC'

/**
 * custom Phaser Scene class
 * provides direct access to custom game components
 */
export default class SceneC extends Phaser.Scene {

  public levelMgr: LevelMgr
  public detection: Detection
  public grad: Grad
  public grid: Grid
  public inputControl: InputControl
  public paths: Paths
  public gameState: GameState

  /**
   * run at start of create() of scene
   */
  public initComponents: () => void

  constructor(sceneKey: string) {

    super(sceneKey)

    this.initComponents = () => {
      const g = this.game as GameC
      this.levelMgr = g.levelMgr
      this.detection = g.detection
      this.grad = g.grad
      this.grid = g.grid
      this.inputControl = g.inputControl
      this.paths = g.paths
      this.gameState = g.gameState
    }

  }
}