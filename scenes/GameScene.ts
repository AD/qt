import DataStore from '../game components/DataStore'
import Starburst from '../ui elements/Starburst'
import Level from '../game components/Level'
import Grid from '../game components/Grid'
import setupAnims from '../setup/setupAnims'
import Sim from '../simulation/Sim'
import StepSim from '../simulation/StepSim'
import Trace from '../simulation/Trace'
import MenuScene from './MenuScene'
import { ATLAS_1, BACK_SCENE, CURRENT_UNLOCKED_LEVEL, END_STEP, GAME_SCENE, LOAD_SCENE, MENU_SCENE, SETUP_LEVEL } from '../setup/strings'
import SceneC from './SceneC'

export default class GameScene extends SceneC {

  public allGamePieces: Phaser.GameObjects.Group

  // the flash on cell as a game piece is dragged over it
  public hilite: Phaser.GameObjects.Image

  // shows corners of cell as game piece is dragged over it
  public cornerFrame: Phaser.GameObjects.Image

  /**
   * adds object to the allObj group
   */
  public addObj: (obj: Phaser.GameObjects.GameObject) => void

  /**
   * deletes all objects in allObj group
   */
  public clearObj: () => void

  /**
   * deletes all objects and all gamepieces
   */
  public clearAllObj: () => void

  /**
   * called from Sim on successful level completion
   */
  public levelComplete: (n: number) => void

  /**
   * called when LevBtn pressed
   */
  public levelMenu: () => void

  /**
   * called from Detector on success
   * Starburst is one common particle emitter for all
   */
  public playStarburst: (x: number, y: number) => void

  constructor() {
    super(GAME_SCENE)
  }

  create() {

    const sc = this
    const g = sc.game
    sc.initComponents()
    let ds = new DataStore()

    sc.scene.remove(LOAD_SCENE)

    sc.matter.world.setBounds().disableGravity()
    sc.matter.world.pause()

    Phaser.GameObjects.BitmapText.ParseFromAtlas(sc, 'level', ATLAS_1, 'level2', 'level2')
    Phaser.GameObjects.BitmapText.ParseFromAtlas(sc, 'percent', ATLAS_1, 'percent2', 'percent2')

    setupAnims(sc)

    sc.hilite = sc.add.image(0, 0, ATLAS_1, 'hilite').setDepth(201).setAlpha(0)
    sc.cornerFrame = sc.add.image(0, 0, ATLAS_1, 'select').setDepth(202).setAlpha(0)
    let allObj = sc.add.group()
    sc.allGamePieces = sc.add.group()

    new Grid()
    new Level(sc)
    new Trace(sc)
    new Sim(sc)
    sc.grad.initG()

    let starburst = new Starburst(sc)

    sc.levelMgr.setCurLevel(parseInt(ds.getStoredData(CURRENT_UNLOCKED_LEVEL)))
    let menuScene = sc.scene.get(MENU_SCENE) as MenuScene

    sc.addObj = function (obj) {
      allObj.add(obj)
    }

    sc.clearObj = function () {
      allObj.clear(true, true)
    }

    sc.clearAllObj = function () {
      allObj.clear(true, true)
      sc.allGamePieces.clear(true, true)
    }

    sc.levelComplete = function (n: number) {
      ds.setStoredData(CURRENT_UNLOCKED_LEVEL, (n + 1) + '')
      sc.inputControl.setInputScene(MENU_SCENE)
      sc.events.emit(END_STEP)
      sc.scene.wake(MENU_SCENE)
      menuScene.levelComplete(n)
    }

    sc.levelMenu = function () {
      sc.inputControl.setInputScene(MENU_SCENE)
      sc.events.emit(END_STEP)
      sc.scene.wake(MENU_SCENE)
    }

    sc.playStarburst = function (x, y) {
      starburst.playG(x, y)
    }

    sc.events.emit(SETUP_LEVEL)

  }

}

new GameScene()