import Button from '../ui elements/Button'
import GameC from '../GameC'
import { TITLE_ATLAS, BOOT_SCENE, LOAD_SCENE } from '../setup/strings'
import SceneC from './SceneC'

/**
 * First scene that runs.
 */
export default class BootScene extends SceneC {
  constructor() {
    super(BOOT_SCENE)
  }

  preload() {
    this.load.setPath('assets/images/')
    this.load.atlas(TITLE_ATLAS, 'qg2.png', 'qg2.json')
  }
  create() {

    const sc = this
    sc.initComponents()

    // for accessing device flags
    const g = sc.game as GameC

    sc.add.image(640, 75, TITLE_ATLAS, 'msg')
    sc.add.image(640, 264, TITLE_ATLAS, 'title')
    let gameBtn = new Button(sc, 640, 540, TITLE_ATLAS, 'game')
    gameBtn.on('pointerup', function () {
      sc.gameState.gameMode = true
      init()
    })

    function init() {
      gameBtn.destroy()
      sc.scene.launch(LOAD_SCENE)
      if (g.device.os.iPhone || g.device.os.android) {
        if (g.device.fullscreen.available) {
          sc.scale.startFullscreen()
        }
      }
    }

    gameBtn.enableBtn()

  }
}