import { GAME_HEIGHT, GAME_WIDTH } from "../../setup/const";
import { MENU_SCENE, MODAL_ATLAS, MODAL_SCENE } from "../../setup/strings";
import ModalScene from "./ModalScene";

export default function (sc: Phaser.Scene) {

  const mod = sc.scene.get(MODAL_SCENE) as ModalScene

  mod.setupModal(480, 560, MENU_SCENE, true)

  let img = new Phaser.GameObjects.Image(mod, GAME_WIDTH / 2, GAME_HEIGHT / 2, MODAL_ATLAS, 'help')
  mod.addObj(img)

}
