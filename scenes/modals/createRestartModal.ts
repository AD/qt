import GameC from "../../GameC";
import { MENU_SCENE, MODAL_ATLAS, MODAL_SCENE } from "../../setup/strings";
import Button from "../../ui elements/Button";
import MenuScene from "../MenuScene";
import ModalScene from "./ModalScene";

export default function createRestartModal(sc: Phaser.Scene) {

  const mod = sc.scene.get(MODAL_SCENE) as ModalScene
  mod.setupModal(400, 240, MENU_SCENE, false)

  // add text image
  let txt = new Phaser.GameObjects.Image(mod, 640, 311, MODAL_ATLAS, 'rm_txt')
  mod.addObj(txt)

  let yesBtn = new Button(mod, 550, 414, MODAL_ATLAS, 'yes')
  mod.addObj(yesBtn)
  yesBtn.on('pointerup', function () {
    mod.close()
    const g = sc.game as GameC
    g.levelMgr.setCurLevel(1)
    let menu = sc.scene.get(MENU_SCENE) as MenuScene
    menu.setCurLevelTo(1)
  })
  yesBtn.enableBtn()

  let noBtn = new Button(mod, 730, 414, MODAL_ATLAS, 'no')
  mod.addObj(noBtn)
  noBtn.on('pointerup', function () {
    mod.close()
  })
  noBtn.enableBtn()

}