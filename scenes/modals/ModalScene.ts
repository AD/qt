import GameC from "../../GameC"
import { GAME_HEIGHT, GAME_WIDTH } from "../../setup/const"
import { MODAL_ATLAS, MODAL_SCENE } from '../../setup/strings'

export default class ModalScene extends Phaser.Scene {


  public setupModal: (width: number, height: number, returnSceneKey: string, clickAnywhereToClose?: boolean) => void

  /**
   * for adding objects, construct then pass to this function
   */
  public addObj: (obj: Phaser.GameObjects.GameObject) => void

  /**
   * if using buttons to close, call this function from click
   */
  public close: () => void

  constructor() {
    super(MODAL_SCENE)
  }

  create() {
    const sc = this
    const g = this.game as GameC

    let allObj: Phaser.GameObjects.GameObject[] = []

    // full size darkening base
    sc.add.image(GAME_WIDTH / 2, GAME_HEIGHT / 2, MODAL_ATLAS, 'levbase').setDisplaySize(GAME_WIDTH, GAME_HEIGHT).setAlpha(.9)
    // solid black at defined size
    let base = sc.add.image(GAME_WIDTH / 2, GAME_HEIGHT / 2, MODAL_ATLAS, 'levbase')

    sc.setupModal = (width, height, returnSceneKey, clickAnywhereToClose?) => {

      base.setDisplaySize(width, height)

      g.inputControl.setInputScene(MODAL_SCENE)

      sc.close = () => {
        sc.input.off('pointerup', sc.close)
        for (let obj of allObj) {
          obj.destroy()
        }
        sc.scene.sleep()
        g.inputControl.setInputScene(returnSceneKey)
      }

      // close on click anywhere
      if (clickAnywhereToClose) {
        // if (clickAnywhereToClose) {
        sc.input.on('pointerup', sc.close)
      }


      sc.scene.wake()

    }

    sc.addObj = (obj) => {
      sc.add.existing(obj)
      allObj.push(obj)
    }

    sc.scene.sleep()

  }

}