import Mine from "../game pieces/types/Mine";
import fadeOut from "../tweens/fadeOut";
import GameScene from '../scenes/GameScene';
import GamePiece from '../game pieces/GamePiece';
import { CLEAR_TRACERS, NONE, SETUP_LEVEL, STOP_SIM } from '../setup/strings';

export default function (sc: GameScene, mine: Mine) {

  sc.events.emit(STOP_SIM, true)
  sc.events.emit(CLEAR_TRACERS, true)

  const finish = () => {
    sc.input.off('pointerdown', finish)
    sc.time.removeAllEvents()
    sc.matter.world.pause()
    sc.events.emit(SETUP_LEVEL)
  }

  sc.input.on('pointerdown', finish)

  sc.clearObj()

  sc.allGamePieces.children.each(function (gp: GamePiece) {
    gp.setShape()
    gp.setBounce(.5)
  })

  mine.setStatic(true)
  mine.setScale(1.5)
  mine.anims.play('expl')

  sc.matter.world.resume()

  let mx = mine.x
  let my = mine.y

  sc.allGamePieces.children.each(function (gp: GamePiece) {
    let x = gp.x - mx + Phaser.Math.RND.between(-10, 10)
    let y = gp.y - my + Phaser.Math.RND.between(-10, 10)
    let force = new Phaser.Math.Vector2(x, y)
    let length = force.length()
    let delay = length / 5
    force.normalize()
    force.scale(.4)
    sc.time.delayedCall(delay, function () {
      gp.applyForce(force)
    })
    fadeOut(sc, gp, 700, 3000)
  })

  sc.time.delayedCall(4000, function () {
    finish()
  })



}