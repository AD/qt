import GameScene from '../scenes/GameScene'
import { ATLAS_1 } from '../setup/strings'

export default class Photon extends Phaser.GameObjects.Image {
  constructor(sc: GameScene, x: number, y: number, frame: string) {
    super(sc, x, y, ATLAS_1, frame)
    sc.add.existing(this)
  }
}
