// TODO hide dom when level scene shows

import GameScene from '../scenes/GameScene'
import { CLEAR_TRACERS, END_STEP, STOP_SIM, TRACE } from '../setup/strings'
import StepPhoton from "./StepPhoton"

export default class StepSim {

  constructor(sc: GameScene) {

    let activeStepIndex = 0
    let lastStep = 0
    let activePhots: { [k: string]: StepPhoton } = {}
    let stepIDs: string[][] = []

    function init() {
      sc.events.emit(STOP_SIM)
      sc.events.emit(CLEAR_TRACERS)
      sc.gameState.stepping = true
      activeStepIndex = 0
      let count = sc.steps.getCount()
      lastStep = count - 2

      // clear any leftovers in same level
      for (let p in activePhots) {
        activePhots[p].destroyElem()
        activePhots[p].destroy()
        delete activePhots[p]
      }

      // create array of array of pathIDs for each step 
      stepIDs.length = 0
      for (let i = 0; i < count; i++) {
        stepIDs[i] = []
        let step = sc.steps.getStep(i)
        for (let pathID in step) {
          stepIDs[i].push(pathID)
        }
      }

      sc.matter.resume()

      stepForw()
    }

    function stepForw() {

      // console.clear()

      if (sc.gameState.inStep) {
        return
      }
      if (!sc.gameState.stepping) {
        init()
        return
      }
      sc.gameState.inStep = true

      let nextStepIndex = activeStepIndex + 1

      let endShowNext: { (): void }

      if (activeStepIndex < lastStep) {

        // NOTE this is needed if photon image is to rotate
        // if (nstep.param === 'y') {
        //   nxtGrp[i].setAngle(90)
        // }

        endShowNext = function () {
          // if current ID not in next step, recycle
          for (let id of stepIDs[activeStepIndex]) {
            if (stepIDs[nextStepIndex].indexOf(id) === -1) {
              if (activePhots.hasOwnProperty(id)) {
                activePhots[id].hideG()
              } else {
                // console.log('endShowNext: id', id, 'is not in activePhots')
              }
            }
          }
          // if next ID not in current step, create new
          for (let id of stepIDs[nextStepIndex]) {
            if (stepIDs[activeStepIndex].indexOf(id) === -1) {
              // TODO create new
              let phot = getPhot(nextStepIndex, id)
              phot.showG()
            }
          }
          // if ID exists in current and next, do nothing
          activeStepIndex++
          sc.gameState.inStep = false
        }

      } else {
        endShowNext = function () {
          end()
          sc.events.emit(TRACE)
        }
      }


      // create the moving parts 

      let curStep = sc.steps.getStep(activeStepIndex)
      let first = true

      for (let id of stepIDs[activeStepIndex]) {

        let phot = getPhot(activeStepIndex, id)

        let param = curStep[id].param
        let startV
        if (param === 'x') {
          startV = curStep[id].startX
        } else {
          startV = curStep[id].startY
          // NOTE needed for photon images that require rotation
          // moveGroup[i].setAngle(90)
        }
        let endV = startV + curStep[id].delta

        // set show next only for first photon if many
        let tw = stepTo(phot, param, endV)

        if (first) {
          tw.on('complete', function () {
            endShowNext()
          })
          first = false
        }
      }

      // TODO check if current activePhot list matches stepID list

      // if (activeStepIndex === 0) {
      //   sc.events.emit('enablestepback')
      // }
    }

    function stepBack() {

      // console.clear()

      if (sc.gameState.inStep) {
        return
      }

      activeStepIndex--
      if (activeStepIndex < 0) {
        activeStepIndex = 0
        return
      }

      sc.gameState.inStep = true

      let prevStepIndex = activeStepIndex + 1

      // remove the ones that have ended
      for (let id of stepIDs[prevStepIndex]) {
        if (stepIDs[activeStepIndex].indexOf(id) === -1) {
          if (activePhots.hasOwnProperty(id)) {

            activePhots[id].hideG()
          } else {
            // console.log('id', id, 'not in activePhots')
          }
        }
      }

      let curStep = sc.steps.getStep(activeStepIndex)
      let first = true

      for (let id of stepIDs[activeStepIndex]) {

        let endV = curStep[id].startX

        let phot = getPhot(activeStepIndex, id)

        let param = curStep[id].param
        if (param === 'y') {
          endV = curStep[id].startY
          // photon.setAngle(90)
        }
        let startV = endV + curStep[id].delta
        if (param === 'y') {
          phot.y = startV
        } else {
          phot.x = startV
        }

        let tw = stepTo(phot, param, endV)
        if (first) {
          if (activeStepIndex === 0) {
            tw.on('complete', function () {
              end()
              sc.events.emit(TRACE)
            })
          } else {
            tw.on('complete', function () {
              sc.gameState.inStep = false
            })
          }
          first = false
        }
      }



    }

    function end() {
      sc.matter.pause()
      sc.gameState.inStep = false
      sc.gameState.stepping = false
      activeStepIndex = 0
      // sc.events.emit('disablestepback')
      // if (nxtGrp.length > 0) {
      //   for (let mem of nxtGrp) {
      //     mem.destroy()
      //   }
      // nxtGrp.length = 0
      // }
      for (let p in activePhots) {
        activePhots[p].destroyElem()
        activePhots[p].destroy()
        delete activePhots[p]
      }
    }

    function getPhot(stepIndex: number, pathID: string) {
      let step = sc.steps.getStep(stepIndex)[pathID]
      if (activePhots.hasOwnProperty(pathID)) {
        // console.log(step)
        activePhots[pathID].setPos(step.startX, step.startY)
        return activePhots[pathID]
      }
      let phot = new StepPhoton(sc)
      phot.updateDisplay(step.ha, step.va, step.hp, step.vp, step.dir)
      phot.setPos(step.startX, step.startY)
      activePhots[pathID] = phot
      return phot
    }

    function stepTo(obj: StepPhoton, param: string, endV: number) {

      obj.startAnim()
      if (param === 'x') {
        return sc.tweens.add({
          targets: obj,
          duration: 200,
          ease: 'Cubic.easeInOut',
          x: endV
        })
      } else {
        return sc.tweens.add({
          targets: obj,
          duration: 200,
          ease: 'Cubic.easeInOut',
          y: endV
        })
      }
    }


    sc.events.on(END_STEP, end)
    // sc.events.on('stepback', stepBack)
    // sc.events.on('stepforw', stepForw)

    sc.input.keyboard.on('keydown_LEFT', stepBack)
    sc.input.keyboard.on('keydown_RIGHT', stepForw)

  }
}








