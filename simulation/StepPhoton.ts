import getPer from "../game pieces/functions/getPer";
import GameScene from '../scenes/GameScene';
import { GAME_HEIGHT, GAME_WIDTH, PI2 } from "../setup/const";
import { ATLAS_1 } from "../setup/strings";
import { getDisplayNum } from "./functions";

const domStyle = {
  'background-color': 'rgba(0,0,0,.8)',
  'padding': '4px',
  'font': '12px monospace',
  'color': 'white',
  'text-align': 'center',
  'border-style': 'solid',
  'border-width': '1px',
  'border-color': 'rgba(255,255,255,.5)'
};

const lineStyle = {
  'width': '27px',
  'height': '1px',
  'border-style': 'solid',
  'border-width': '1px 0 0 0',
  'border-color': 'rgba(255,255,255,.7)'
}

const elemDirs = [
  [-20, -20],
  [20, -20],
  [20, 20],
  [-20, 20]
]

const arrows = ['→', '↓', '←', '↑']

export default class StepPhoton extends Phaser.GameObjects.Sprite {

  public updateDisplay: (ha: number, va: number, hp: number, vp: number, stepDir: number) => void
  public showG: () => void
  public hideG: () => void
  public startAnim: () => void
  public setPos: (x: number, y: number) => void
  public destroyElem: () => void

  constructor(sc: GameScene) {

    super(sc, -100, -100, ATLAS_1, 'beam-head')

    this.visible = false
    this.depth = 10

    let elemX: number = 0
    let elemY: number = 0
    let dir: number = 0
    let minX: number = 0
    let minY: number = 0
    let maxX: number = 0
    let maxY: number = 0

    let style1 = '<span style="color:rgb(145,255,0)">'
    let style2 = '<span style="color:rgb(0,90,255);font-size:12px">'

    let elem = sc.add.dom(0, 0, 'div', domStyle, 'default').setVisible(false)
    sc.addObj(elem)

    let line = sc.add.dom(0, 0, 'div', lineStyle).setVisible(false)
    line.setOrigin(1, .5)
    sc.addObj(line)

    this.destroyElem = function () {
      elem.destroy()
      line.destroy()
    }

    this.updateDisplay = function (ha, va, hp, vp, stepDir) {
      dir = stepDir

      let str = ''
      if (ha > 0) {
        str += style1 + 'H</span>' + style2 + arrows[dir] + '</span> ' + getDisplayNum(ha) + '<br>'
        str += 'exp(' + getDisplayNum(hp * PI2) + 'i)'
      }
      if (va > 0) {
        // add br if H values added 
        if (ha > 0) {
          str += "<br>"
        }
        str += style1 + 'V</span>' + style2 + arrows[dir] + '</span> ' + getDisplayNum(va) + '<br>'
        str += 'exp(' + getDisplayNum(vp * PI2) + 'i)'
      }

      elem.setHTML(str)
      if (dir === 0 || dir === 3) {
        elem.displayOriginX = elem.width
        maxX = GAME_WIDTH
        minX = elem.width
      } else {
        elem.displayOriginX = 0
        maxX = GAME_WIDTH - elem.width
        minX = 0
      }
      if (dir === 0 || dir === 1) {
        elem.displayOriginY = elem.height
        maxY = GAME_HEIGHT
        minY = elem.height
      } else {
        elem.displayOriginY = 0
        maxY = GAME_HEIGHT - elem.height
        minY = 0
      }

      let tint = sc.grad.getTintG(getPer(ha, va))
      this.setTint(tint)

    }

    this.setPos = function (x, y) {
      this.setPosition(x, y)
      elemX = elemDirs[dir][0]
      elemY = elemDirs[dir][1]
      elem.x = this.x + elemX
      elem.y = this.y + elemY
      checkBounds()
      line.x = this.x
      line.y = this.y
      line.angle = 45 + (90 * dir)
    }

    this.showG = function () {
      this.visible = true
      elem.visible = true
      line.visible = true
    }

    this.hideG = function () {
      this.visible = false
      elem.visible = false
      line.visible = false
      this.preUpdate = function () { }
    }

    this.startAnim = function () {
      this.showG()
      this.preUpdate = function () {

        elem.x = this.x + elemX
        elem.y = this.y + elemY
        checkBounds()

        line.x = this.x
        line.y = this.y

      }
    }

    function checkBounds() {
      if (elem.x > maxX) { elem.x = maxX }
      if (elem.x < minX) { elem.x = minX }
      if (elem.y > maxY) { elem.y = maxY }
      if (elem.y < minY) { elem.y = minY }
    }

    sc.add.existing(this)
    sc.addObj(this)

  }

}