import GameScene from '../scenes/GameScene';
import { ATLAS_1 } from '../setup/strings';

export default class TracerPool {

  public addTracer: (col: number, row: number, dir: number, per: number, legal: boolean, pol: string) => void
  public resetTracers: () => void

  constructor(sc: GameScene) {

    let pool: Tracer[] = []
    let available: number = -1

    this.addTracer = (col, row, dir, per, legal, pol) => {

      if (available >= 0) {
        pool[available].initG(col, row, dir, per, legal, pol)
        available--

      } else {
        let t = new Tracer(sc)
        t.initG(col, row, dir, per, legal, pol)
        pool.push(t)

      }
    }
    this.resetTracers = () => {
      for (let t of pool) {
        t.recycle()
      }
      available = pool.length - 1
    }
  }

}

const animAngles = [0, 90, 180, -90]

class Tracer extends Phaser.GameObjects.Sprite {

  public recycle: () => void
  public initG: (col: number, row: number, dir: number, per: number, legal: boolean, polStr: string) => void

  constructor(sc: GameScene) {
    super(sc, -100, -100, ATLAS_1)

    const t = this

    t.initG = (col, row, dir, per, legal, polStr) => {
      t.isCropped = false
      t.clearTint()
      t.x = sc.grid.getCellX(col)
      t.y = sc.grid.getCellY(row)
      t.setOrigin(0, .5)
      if (!legal) {
        t.setCrop(0, 0, 40, 10)
      }
      t.setAngle(animAngles[dir])
      t.alpha = 1
      per = Phaser.Math.RoundTo(per, -4)
      t.setDepth(Math.round(per * 100))
      let anim = ""
      if (per === 1) {
        anim = "yl" + polStr
      } else if (per === .5) {
        anim = "rd" + polStr
      } else if (per === .25) {
        anim = "pr" + polStr
      } else if (per === .125) {
        anim = "bl" + polStr
      } else {
        anim = "wh" + polStr
        t.setTint(sc.grad.getTintG(per))
      }
      t.anims.play(anim)

    }

    t.recycle = () => {
      t.x = -100
      t.y = -100
      t.anims.stop()
      t.alpha = 0
    }

    sc.add.existing(t)

  }

}