import getPer from "../game pieces/functions/getPer"
import GamePiece from "../game pieces/GamePiece"
import GameScene from '../scenes/GameScene'
import { HALF_CELL, HALF_SEGMENT_DURATION, SEGMENT_DURATION } from '../setup/const'
import levels from "../setup/levels"
import { BACK_SCENE, CLEAR_TRACERS, DETECTOR, EMPTY, END_STEP, GAME_SCENE, LASER, MINE, NONE, POLAR_SPLITTER, TRACE } from '../setup/strings'
import { INext } from "../setup/types"
import { processPhases, getPol } from "./functions"
import TracerPool from "./TracerPool"

export default class {

  constructor(sc: GameScene) {

    const dirs = [
      [1, 0],
      [0, 1],
      [-1, 0],
      [0, -1]
    ]

    // safety counter
    const maxLimit = 240
    let limit = 0

    let stepCount = 0
    let partID = 0
    let tracersOn: boolean = true
    let tracers = new TracerPool(sc)
    let resetTracers = tracers.resetTracers

    let round = Phaser.Math.RoundTo

    function trace() {

      sc.inputControl.setInputScene(NONE)

      if (sc.levelMgr.isTraceOn() && sc.gameState.gameMode) {
        tracersOn = true
      } else {
        tracersOn = false
      }

      if (sc.gameState.stepping) {
        sc.events.emit(END_STEP)
      }

      resetTracers()

      sc.detection.resetPer()
      sc.detection.hideAlerts()

      sc.allGamePieces.children.each(function (gp: GamePiece) {
        gp.hideAbsorb()
      })

      // todo these can be stored somewhere once at the start of each level
      let start = levels[sc.levelMgr.getCurLevel()].laserDef
      let col = start[0]
      let row = start[1]
      let dir = start[2] / 2

      // default
      let ha = 1
      let va = 0
      // NOTE only one other option for starting polarity 
      if (start[3] === 6) {
        ha = 0
        va = 1
      }

      limit = 0
      stepCount = 0
      partID = 0

      sc.paths.resetPaths()
      sc.steps.resetSteps()
      sc.steps.addStep(0)

      // first beam
      if (tracersOn) {
        tracers.addTracer(col, row, dir, 1, true, getPol(ha, va, 0, 0))
      }
      let ndata: INext = {
        col: col,
        row: row,
        dir: dir,
        ha: ha,
        va: va,
        per: 1, // hard coding for now
        hp: 0,
        vp: 0,
        id: partID
      }
      let data = addToNextArr(ndata)

      if (data !== undefined) {
        next([data])
      }

    }

    let next = (arr: INext[]) => {
      let nextArr: INext[] = []
      stepCount++
      sc.steps.addStep(stepCount)

      // first check for any interference pairs
      let checkedArr = []
      let len = arr.length
      if (len > 1) {
        for (let i = 0; i < len - 1; i++) {
          for (let j = i + 1; j < len; j++) {
            if (superExists(arr[i], arr[j])) {
              checkedArr.push(i)
              checkedArr.push(j)
              superPair(arr[i], arr[j])
            }
          }
        }
      }

      // then process all others
      for (let i = 0; i < len; i++) {
        if (checkedArr.indexOf(i) === -1) {
          single(arr[i])
        }
      }

      // safety cutoff
      if (++limit > maxLimit) {
        return
      }

      // after all checked, if all haven't stopped, run again 
      if (nextArr.length !== 0) {
        next(nextArr)
      } else {
        sc.inputControl.setInputScene([GAME_SCENE, BACK_SCENE])
      }

      function superPair(data1: INext, data2: INext) {


        let obj: GamePiece = sc.grid.getObjOnGrid(data1.row, data1.col)

        let res1 = obj.process(data1)
        let res2 = obj.process(data2)

        // TODO this is wrong
        // one could get absorbed while the other passes through 
        // TODO this is only if object is splitter 
        if (res1[0].dir === -1 || res2[0].dir === -1) {
          if (res1[0].dir !== -1) {
            setupNext(res1[0])
          }
          if (res2[0].dir !== -1) {
            setupNext(res2[0])
          }
          return
        }

        // there is a possibility of no interference 
        // if dirs are opposite and angle is 45 
        // TODO this hasn't been tested - is it possible on any level? 
        if (Math.abs(data1.dir - data2.dir) === 2) {
          // polarSplitter is always 45 deg
          if (obj.rType === POLAR_SPLITTER || (obj.ang === 1 || obj.ang === 3)) {
            // console.log('passing through without interference')
            setupNext(res1[0])
            setupNext(res1[1])
            setupNext(res2[0])
            setupNext(res2[1])
            return
          }
        }

        // now there will be only 2 dirs with 2 inputs each 
        let pair1a = res1[0]
        let pair2a = res1[1]
        let pair1b
        let pair2b
        if (res2[0].dir === pair1a.dir) {
          pair1b = res2[0]
          pair2b = res2[1]
        } else {
          pair1b = res2[1]
          pair2b = res2[0]
        }
        if (pair1b === undefined) {
          // console.log(res1)
          // throw new Error(res2)
        }

        superBranch(pair1a, pair1b)
        superBranch(pair2a, pair2b)

      }

      function superBranch(pA: INext, pB: INext) {


        let newHp = processPhases(pA.hp, pB.hp)
        let newVp = processPhases(pA.vp, pB.vp)

        pA.ha = Math.sqrt(pA.ha * pA.ha + pB.ha * pB.ha + 2 * pA.ha * pB.ha * Math.cos(newHp.diff))
        pA.ha = round(pA.ha, -10)
        pA.va = Math.sqrt(pA.va * pA.va + pB.va * pB.va + 2 * pA.va * pB.va * Math.cos(newVp.diff))
        pA.va = round(pA.va, -10)
        pA.per = getPer(pA.ha, pA.va)
        if (pA.ha > 0) {
          pA.hp = newHp.ph
        } else {
          pA.hp = 0
        }
        if (pA.va > 0) {
          pA.vp = newVp.ph
        } else {
          pA.vp = 0
        }

        // console.log('result', pA.ha, pA.va, pA.per, pA.dir)
        setupNext(pA)
      }

      function single(data: INext) {

        let obj: GamePiece = sc.grid.getObjOnGrid(data.row, data.col)

        let per = data.per
        let next = nextCell(data.col, data.row, data.dir)
        let pol = getPol(data.ha, data.va, data.hp, data.vp)

        if (obj === null || obj.rType === EMPTY) {
          if (tracersOn) {
            tracers.addTracer(data.col, data.row, data.dir, Math.round(per * 10000) / 10000, next.legal, pol)
          }
          // blank space
          if (obj === null) {
            let data1 = addToNextArr(data)
            if (data1 !== undefined) {
              nextArr.push(data1)
            }
            return
          }

        }

        if (obj.rType === LASER) {
          return
        }
        if (obj.rType === DETECTOR) {
          obj.per += per
          if (data.dir * 2 === obj.ang) {
            obj.absorb()
          }
          sc.paths.addDetector("p" + data.id, obj)
          return
        }
        if (obj.rType === MINE) {
          sc.paths.addMine(sc, "p" + data.id, obj)
          return
        }

        // else 
        let result = obj.process(data)
        for (let res of result) {
          setupNext(res)
        }
      }

      function setupNext(result: INext) {

        let res = Object.assign({}, result)

        if (res.dir === -1) { // object acting as endpoint, like perpendicular reflector
          return
        }
        // NOTE this is arbitrary 
        if (res.per < .01) {
          return
        }
        partID++
        res.id = partID
        let pol = getPol(res.ha, res.va, res.hp, res.vp)

        if (tracersOn) {
          let next = nextCell(res.col, res.row, res.dir)
          tracers.addTracer(res.col, res.row, res.dir, Math.round(res.per * 10000) / 10000, next.legal, pol)
        }

        let nextData = addToNextArr(res)

        if (nextData !== undefined) {
          nextArr.push(nextData)
        }
      }

    }

    let superExists = (data1: INext, data2: INext) => {

      if (data1.col === data2.col && data1.row === data2.row) {
        if (sc.grid.getObjOnGrid(data1.row, data1.col) !== null) {
          if (sc.grid.getObjOnGrid(data1.row, data1.col).isSplitter) {
            return true
          }
        }
      }

      return false
    }

    let addToNextArr = (data: INext): INext => {

      let nextLocation = nextCell(data.col, data.row, data.dir)

      let startX = sc.grid.getCellX(data.col)
      let startY = sc.grid.getCellY(data.row)
      let param
      let endV

      let pathID = 'p' + data.id

      if (nextLocation.legal) {

        if (nextLocation.col === data.col) {
          param = 'y'
          endV = sc.grid.getCellY(nextLocation.row)

        } else {
          param = 'x'
          endV = sc.grid.getCellX(nextLocation.col)

        }

        sc.paths.addPath(pathID, {
          startT: stepCount * SEGMENT_DURATION,
          startX: startX,
          startY: startY,
          param: param,
          dur: SEGMENT_DURATION,
          endV: endV,
          per: data.per
        })

        data.col = nextLocation.col
        data.row = nextLocation.row

        return data

      }

      else {

        let addVal = HALF_CELL
        if (data.dir === 2 || data.dir === 3) {
          addVal = -addVal
        }

        if (sc.paths.idExists(pathID)) {
          sc.paths.closePath('p' + data.id, addVal)
        }

        else {
          partID++
          if (data.dir === 0 || data.dir === 2) {
            endV = startX + addVal
            param = 'x'
          } else {
            endV = startY + addVal
            param = 'y'
          }
          sc.paths.addPath('p' + partID, {
            startT: stepCount * SEGMENT_DURATION,
            startX: startX,
            startY: startY,
            param: param,
            dur: HALF_SEGMENT_DURATION,
            endV: endV,
            per: data.per
          })
        }
      }
    }

    let nextCell = (col: number, row: number, dir: number) => {

      let cell = {
        legal: true,
        row: row,
        col: col
      }

      col += dirs[dir][0]
      row += dirs[dir][1]

      if (col < 0 || row < 0 || col >= sc.grid.getNumCols() || row >= sc.grid.getNumRows()) {
        cell.legal = false
        return cell
      }

      if (sc.grid.getObjOnGrid(row, col) !== null) {
        if (sc.grid.getObjOnGrid(row, col).rType === EMPTY) {
          cell.legal = false
          return cell
        }
      }

      cell.row = row
      cell.col = col

      return cell

    }

    sc.events.on(CLEAR_TRACERS, function () {
      tracers.resetTracers()
    })
    sc.events.on(TRACE, trace)
  }


}