import fadeOut from "../tweens/fadeOut"
import GameScene from '../scenes/GameScene'
import { ATLAS_1, CLEAR_TRACERS, END_STEP, START_SIM, STOP_SIM, TRACE } from '../setup/strings'

export default class Sim {

  constructor(sc: GameScene) {

    let eventCount: number = 0
    let startTime: number = 0
    let timeElapsed: number = 0
    let endTime: number = 0
    let fnCount: number = 0
    let simObj = sc.add.group({
      classType: Phaser.GameObjects.Image,
      defaultKey: ATLAS_1
    })
    let sequenceEvents: {
      t: number,
      f: {
        (): void
      }
    }[] = []
    let tweens: {
      [k: string]: {
        (): void
      }
    } = {}

    function run() {

      if (sc.gameState.simIsOn) {
        return
      } else {
        sc.gameState.simIsOn = true
      }
      if (sc.gameState.stepping) {
        sc.events.emit(END_STEP)
      }

      sc.events.emit(CLEAR_TRACERS)
      sc.detection.hideAlerts()

      for (let k in tweens) {
        delete tweens[k]
      }

      startTime = 0;
      timeElapsed = 0;
      endTime = 0;
      fnCount = 0;
      sequenceEvents.length = 0
      sequenceEvents = [];
      eventCount

      let _paths = sc.paths.getG()
      for (let path in _paths) {
        let p = _paths[path]
        add(p.startT, p.startX, p.startY, p.param, p.dur, p.endV, p.per)
        if (p.endFn !== undefined) {
          let startT = p.startT + p.dur
          addFn(p.endFn, startT)
        }
      }

      play()

    }

    function add(startT: number, startX: number, startY: number, param: string, dur: number, endV: number, per: number) {

      let obj = simObj.get(-100, -100).setDepth(10).setActive().setVisible(true).setAlpha(0).setOrigin(0, .5)
      obj.setFrame('beam')
      let head = simObj.get(-100, -100).setDepth(10).setActive().setVisible(true).setAlpha(0)
      head.setFrame('beam-head')

      let startV = 0
      let angle = 0
      let finalWid: number = 0
      let dir = 1

      if (param === 'x') {
        if (endV < startX) {
          finalWid = startX - endV
          angle = 180
          dir = -1
        } else {
          finalWid = endV - startX
        }
      }
      if (param === 'y') {
        if (endV < startY) {
          finalWid = startY - endV
          angle = -90
          dir = -1
        } else {
          finalWid = endV - startY
          angle = 90
        }
      }


      if (startT + dur > endTime) {
        endTime = startT + dur
      }

      let endT = startT + dur;
      let diff = finalWid - startV

      let tweenName = 't' + fnCount

      let tweenFunction = function () {
        if (timeElapsed > endT) {
          obj.displayWidth = finalWid
          head.alpha = 0
          delete tweens[tweenName]
        } else {
          let val = diff * (timeElapsed - startT) / dur + startV
          obj.displayWidth = val
          if (param === 'x') {
            head.x = startX + val * dir
          } else {
            head.y = startY + val * dir
          }
        }
      }

      let tint = sc.grad.getTintG(per)

      let f = function () {
        obj.displayWidth = 1
        obj.x = startX
        head.x = startX
        obj.y = startY
        head.y = startY
        obj.angle = angle
        obj.alpha = 1
        head.alpha = 1
        obj.setTint(tint)
        head.setTint(tint)
        tweens[tweenName] = tweenFunction
      }

      sequenceEvents.push({
        t: startT,
        f: f
      })

      fnCount++

    }

    function addFn(fn: () => void, startT: number) {
      sequenceEvents.push({
        t: startT,
        f: fn
      })
      fnCount++
    }

    function play() {
      sequenceEvents.sort(function (a, b) {
        return a.t - b.t
      })
      eventCount = 0;
      startTime = sc.game.loop.getDurationMS()
      sc.game.events.on('step', f);
    }

    function f() {
      timeElapsed = sc.game.loop.getDurationMS() - startTime;
      for (let k in tweens) {
        tweens[k]()
      }
      eventLauncher()
      if (timeElapsed > endTime) {
        stop(false)
      }
    }

    function eventLauncher() {
      if (eventCount < fnCount) {
        if (sequenceEvents[eventCount].t <= timeElapsed) {
          sequenceEvents[eventCount].f()
          eventCount++
          if (eventCount < fnCount) {
            if (sequenceEvents[eventCount].t <= timeElapsed) {
              eventLauncher()
            }
          }
        }
      }
    }

    function interrupt() {
      if (sc.gameState.simIsOn) {
        sc.game.events.off('step', f);
        simObj.children.iterate(function (obj) {
          simObj.killAndHide(obj)
        })
        sc.gameState.simIsOn = false
        sc.events.emit(TRACE)
      }
    }

    function stop(bomb: boolean) {
      if (sc.gameState.simIsOn) {
        sc.game.events.off('step', f);


        if (sc.detection.success()) {
          simObj.children.iterate(function (obj) {
            simObj.killAndHide(obj)
          })
          // sc.time.delayedCall(500, function () {
          //   sc.fadeAllObj()
          // })
          sc.time.delayedCall(600, function () {
            sc.gameState.simIsOn = false
            sc.levelComplete(sc.levelMgr.getCurLevel())

          })
        }

        else {
          if (!bomb) {
            simObj.children.iterate(function (obj) {
              fadeOut(sc, obj, 500)
            })
            sc.detection.showAlerts()
            sc.time.delayedCall(500, function () {
              simObj.children.iterate(function (obj) {
                simObj.killAndHide(obj)
              })
              sc.gameState.simIsOn = false
            })
          }
        }
      }
    }

    sc.events.on(START_SIM, run)
    sc.events.on(STOP_SIM, interrupt)
  }
}