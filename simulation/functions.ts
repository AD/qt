import { PI, PI2 } from "../setup/const"

export function processPhases(phase1: number, phase2: number) {
  let avg = (phase1 + phase2) / 2 // dec
  let ph1 = phase1 * PI2 // rad
  let ph2 = phase2 * PI2 // rad
  let diff = Math.abs(ph1 - ph2) // rar
  if (diff > PI) { // rad
    diff -= PI // rad
    avg += .5 // dec
  }
  return { diff: diff, ph: avg }
}

export function getPol(ha: number, va: number, hp: number, vp: number) {

  // console.log("ha:" + ha + " va:" + va + " hp:" + hp + " vp:" + vp)

  if (va === 0) {
    // console.log("pol:0")
    return '0'
  }
  if (ha === 0) {
    // console.log("pol:2")
    return '2'
  }
  if (hp > 0) {
    if (vp > 0) {
      // console.log("pol:1")
      return '1'
    } else {
      // console.log("pol:3")
      return '3'
    }
  } else {
    if (vp > 0) {
      // console.log("pol:3")
      return '3'
    } else {
      // console.log("pol:1")
      return '1'
    }
  }
}

export function getDisplayNum(n: number): string {
  let num = n * 100
  let str = Math.round(num).toString()
  while (str.length < 3) {
    str = '0' + str
  }
  return str.slice(0, str.length - 2) + '.' + str.slice(-2)
}