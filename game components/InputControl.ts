// NOTE this could simply be a function but
// created class here in case additional methods 
// get added in the future

export default class InputControl {

  public setInputScene: (keyOfSceneToSetActive: string | string[]) => void

  constructor(game: Phaser.Game) {

    let mgr = game.scene

    this.setInputScene = (keyOfSceneToSetActive) => {
      if (!Array.isArray(keyOfSceneToSetActive)) {
        keyOfSceneToSetActive = [keyOfSceneToSetActive]
      }
      for (let sceneKey in mgr.keys) {
        if (keyOfSceneToSetActive.indexOf(sceneKey) !== -1) {
          mgr.getScene(sceneKey).input.enabled = true
        } else {
          mgr.getScene(sceneKey).input.enabled = false
        }
      }
    }
  }

}