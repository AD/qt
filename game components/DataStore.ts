import { CURRENT_UNLOCKED_LEVEL } from "../setup/strings"

/** 
 * stores and retrives data to local storage
 */
export default class DataStore {

  public getStoredData: (key: string) => string

  public setStoredData: (key: string, val: string) => void

  constructor() {
    let defaults: { [key: string]: string } = {}

    // define default values here 
    defaults[CURRENT_UNLOCKED_LEVEL] = '1'

    this.getStoredData = (key) => {
      let result = window.localStorage.getItem(key)
      if (!result) {
        if (defaults.hasOwnProperty(key)) {
          return defaults[key]
        } else {
          return null
        }
      } else {
        return result
      }
    }

    this.setStoredData = (key, val) => {
      window.localStorage.setItem(key, val);
    }
  }

}