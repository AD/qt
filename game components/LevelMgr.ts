import levels from '../setup/levels'

export default class LevelMgr {

  public getCurLevel: () => number
  public setCurLevel: (n: number) => void
  public isTraceOn: () => boolean

  constructor() {
    // levels are numbered from 1
    let curLevel = 1
    let trace = true
    this.setCurLevel = (n) => {
      curLevel = n
      trace = levels[n].showTrace
    }
    this.getCurLevel = () => curLevel
    this.isTraceOn = () => trace
  }

}