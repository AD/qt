import addDragging from '../game pieces/functions/addDragging'
import addGamePiece from '../game pieces/functions/addGamePiece'
import addRotation from '../game pieces/functions/addRotation'
import Detector from '../game pieces/types/Detector'
import Laser from '../game pieces/types/Laser'
import GameScene from '../scenes/GameScene'
import { STEM_COORDS } from '../setup/const'
import levels from '../setup/levels'
import { ATLAS_1, CLEAR_TRACERS, NONE, SETUP_LEVEL, SET_BACK, STOP_SIM, TRACE } from '../setup/strings'
import MultiTileMesh from '../ui elements/MultiTileMesh'

export default class Level {

  constructor(sc: GameScene) {

    let cam = sc.cameras.main

    let setupLevel = () => {

      cam.setZoom(.01)

      sc.inputControl.setInputScene(NONE)

      sc.events.emit(STOP_SIM)
      sc.clearAllObj()
      sc.events.emit(CLEAR_TRACERS)

      let curLev = sc.levelMgr.getCurLevel()

      sc.events.emit(SET_BACK, [levels[curLev].backTex])

      sc.grid.setupGrid(curLev)
      new MultiTileMesh(sc, sc.grid.getStartX(), sc.grid.getStartY(), levels[curLev].gridFrames, levels[curLev].gridDef)

      sc.detection.resetDetectors()

      // -----------------
      // add fixed objects
      // ----------------- 
      let setup = levels[curLev].gamePieces

      // add laser
      let init = levels[curLev].laserDef
      let _laser = new Laser(sc, sc.grid.getCellX(init[0]), sc.grid.getCellY(init[1]), init[2])
      sc.allGamePieces.add(_laser)

      sc.grid.setObjOnGrid(init[1], init[0], _laser)

      // adding 2 additional occupied spaces for _laser
      if (init[2] === 0) {
        sc.grid.setObjOnGrid(init[1], init[0] - 1, _laser)
        sc.grid.setObjOnGrid(init[1], init[0] - 2, _laser)
      }
      else if (init[2] === 2) {
        sc.grid.setObjOnGrid(init[1] - 1, init[0], _laser)
        sc.grid.setObjOnGrid(init[1] - 2, init[0], _laser)
      }
      else if (init[2] === 4) {
        sc.grid.setObjOnGrid(init[1], init[0] + 1, _laser)
        sc.grid.setObjOnGrid(init[1], init[0] + 2, _laser)
      }
      else if (init[2] === 6) {
        sc.grid.setObjOnGrid(init[1] + 1, init[0], _laser)
        sc.grid.setObjOnGrid(init[1] + 2, init[0], _laser)
      }

      // add detectors (the 'plants')
      for (let init of levels[curLev].detectorDef) {
        let det = new Detector(sc, sc.grid.getCellX(init[0]), sc.grid.getCellY(init[1]), init[2], init[3], init[4], levels[curLev].showTrace)
        sc.grid.setObjOnGrid(init[1], init[0], det)
        sc.detection.addDetector(det)
        sc.allGamePieces.add(det)
        let stem = sc.add.image(sc.grid.getCellX(init[0]), sc.grid.getCellY(init[1]), ATLAS_1, 'stem' + init[5]).setDepth(203)
        let dir = 1
        if (init[6] === true) {
          stem.setFlipX(true)
          dir = -1
        }
        stem.displayOriginX += (STEM_COORDS[init[5]][0] * dir)
        stem.displayOriginY += STEM_COORDS[init[5]][1]
        sc.addObj(stem)
      }

      // add all game pieces 
      for (let type in setup) {
        for (let init of setup[type]) {
          let obj = addGamePiece(sc, type)
          obj.x = sc.grid.getCellX(init[0])
          obj.y = sc.grid.getCellY(init[1])

          if (init[3] === 1 || init[4] === 1) {
            obj.interact()
          }

          if (init[3] === 1) {
            addDragging(sc, obj)
          }

          if (init[4] === 1) {
            addRotation(sc, obj)
            if (init[3] === 0) {
              obj.rotateOnly()
            }
          }

          obj.setAng(init[2])

          sc.grid.setObjOnGrid(init[1], init[0], obj)
        }
      }

      sc.events.emit(TRACE)

      cam.zoomTo(1, 800, 'Quint.easeOut', true)

    }

    sc.events.on(SETUP_LEVEL, setupLevel)
  }
}