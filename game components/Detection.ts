import Detector from "../game pieces/types/Detector";

export default class Detection {

  // Detectors are the plants 

  public resetDetectors: () => void
  public addDetector: (obj: Detector) => void
  public showAlerts: () => void
  public hideAlerts: () => void
  public resetPer: () => void
  public success: () => boolean

  constructor() {
    let detectors: Detector[] = []

    this.resetDetectors = () => {
      detectors.length = 0
    }

    this.addDetector = (obj) => {
      detectors.push(obj)
    }

    this.showAlerts = () => {
      for (let d of detectors) {
        d.showAlert()
      }
    }

    this.hideAlerts = () => {
      for (let d of detectors) {
        d.hideAlert()
      }
    }

    this.resetPer = () => {
      for (let d of detectors) {
        d.per = 0
        d.absorb()
      }
    }

    this.success = () => {
      for (let d of detectors) {
        if (!d.reached()) {
          return false
        }
      }
      return true
    }
  }

}