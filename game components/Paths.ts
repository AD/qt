import { IPath } from '../setup/types'
import GamePiece from '../game pieces/GamePiece'
import explode from '../simulation/explode'
import Detector from '../game pieces/types/Detector'
import GameScene from '../scenes/GameScene'
import { HALF_SEGMENT_DURATION, SEGMENT_DURATION } from '../setup/const'

export default class Paths {

  public getG: () => { [k: string]: IPath }
  public idExists: (id: string) => boolean
  public resetPaths: () => void
  public addPath: (id: string, path: IPath) => void
  public closePath: (id: string, addVal: number) => void
  public addMine: (sc: GameScene, id: string, obj: GamePiece) => void
  public addDetector: (id: string, obj: GamePiece | Detector) => void

  constructor() {
    let _paths: { [k: string]: IPath } = {}
    this.getG = () => _paths
    this.idExists = (id) => _paths.hasOwnProperty(id)
    this.resetPaths = () => {
      for (let k in _paths) {
        delete _paths[k]
      }
    }

    this.addPath = (id, path) => {
      if (!_paths.hasOwnProperty(id)) {
        _paths[id] = path
      } else {
        _paths[id].dur += SEGMENT_DURATION
        _paths[id].endV = path.endV
      }
    }

    this.closePath = (id, addVal) => {
      _paths[id].dur += HALF_SEGMENT_DURATION
      _paths[id].endV += addVal
    }

    this.addMine = (sc, id, obj) => {
      _paths[id].endFn = function () {
        explode(sc, obj)
      }

    }

    this.addDetector = (id, obj) => {
      _paths[id].endFn = function () {
        obj.playStarburst()
        obj.absorb()
      }
    }

  }

}