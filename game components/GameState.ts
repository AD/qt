export default class {
  public simIsOn: boolean = false
  public stepping: boolean = false
  public inStep: boolean = false
  public gameMode: boolean = false
  public soundOn: boolean = true
  public musicOn: boolean = true
}