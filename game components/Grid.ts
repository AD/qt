import { CELL_SIZE, GAME_HEIGHT, GAME_WIDTH } from '../setup/const'
import levels from '../setup/levels'
import { EMPTY } from '../setup/strings'

export default class Grid {

  public getCellX: (n: number) => number
  public getCellY: (n: number) => number

  public setObjOnGrid: (col: number, row: number, obj: any) => void
  public getObjOnGrid: (col: number, row: number) => any

  public getNumCols: () => number
  public getNumRows: () => number

  public getStartX: () => number
  public getStartY: () => number

  public setupGrid: (level: number) => void

  constructor() {

    let cellX: number[] = []
    let cellY: number[] = []
    this.getCellX = (n) => cellX[n]
    this.getCellY = (n) => cellY[n]

    // cells are occupied by a game object or this 'blank' object 
    let dummy = {
      rType: EMPTY
    }

    let objOnGrid: any[][] = [] // cells as [row][col]
    this.setObjOnGrid = (col, row, obj) => {
      objOnGrid[col][row] = obj
    }
    this.getObjOnGrid = (col, row) => {
      return objOnGrid[col][row]
    }

    let numCols = 13
    let numRows = 9
    this.getNumCols = () => numCols
    this.getNumRows = () => numRows

    let gridStartX = 0
    let gridStartY = 0
    this.getStartX = () => gridStartX
    this.getStartY = () => gridStartY

    this.setupGrid = function (lev) {

      cellY.length = 0
      cellX.length = 0
      objOnGrid.length = 0

      // calculate start X & Y
      let gData = levels[lev].gridDef
      numRows = gData.length
      numCols = gData[0].length

      let gridWid = numCols * CELL_SIZE
      let gridHgt = numRows * CELL_SIZE

      gridStartX = Math.round((GAME_WIDTH - gridWid) / 2)
      gridStartY = Math.round((GAME_HEIGHT - gridHgt) / 2)

      let startX = gridStartX + CELL_SIZE / 2
      let startY = gridStartY + CELL_SIZE / 2

      for (let row = 0; row < numRows; row++) {
        cellY[row] = startY + CELL_SIZE * row
        objOnGrid[row] = []
        for (let col = 0; col < numCols; col++) {
          if (row == 0) {
            cellX[col] = startX + CELL_SIZE * col
          }
          if (gData[row][col] === -1) {
            objOnGrid[row][col] = dummy
          } else {
            objOnGrid[row][col] = null
          }
        }
      }
    }
  }
}