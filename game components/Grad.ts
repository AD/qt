// Pre-define colors for 0-100 percent

export default class Grad {

  public initG: () => void
  public getTintG: (per: number) => number

  constructor() {
    let colors: number[] = []

    const Col = Phaser.Display.Color
    const Interpolate = Col.Interpolate.ColorWithColor

    this.initG = () => {
      let tint100 = new Col(253, 194, 20) // 0xfdc214
      let tint50 = new Col(229, 45, 0) // 0xe52d00
      let tint25 = new Col(155, 8, 248) // 0x9b08f8
      let tint125 = new Col(11, 74, 207) // 0x0b4acf
      let tint0 = new Col(74, 82, 86) // 0x4a5256 

      for (let i = 0; i <= 100; i++) {
        let col
        if (i <= 13) {
          col = Interpolate(tint0, tint125, 13, i)
        } else if (i <= 25) {
          col = Interpolate(tint125, tint25, 12, i - 13)
        } else if (i <= 50) {
          col = Interpolate(tint25, tint50, 25, i - 25)
        } else {
          col = Interpolate(tint50, tint100, 50, i - 50)
        }
        colors[i] = Col.GetColor(Math.round(col.r), Math.round(col.g), Math.round(col.b))
      }
    }

    /**
     * @per Value between 0 - 1 
     */
    this.getTintG = (per) => colors[Math.round(per * 100)]
  }

}