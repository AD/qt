import GameScene from '../scenes/GameScene';
import { ATLAS_1 } from '../setup/strings';
import ToggleButton from "./ToggleButton";

export default class extends ToggleButton {
  constructor(sc: GameScene) {
    super(sc, 1240, 680, ATLAS_1, "sound")

    this.setOn = function () {
      sc.sound.mute = false
    }

    this.setOff = function () {
      sc.sound.mute = true
    }

  }
}