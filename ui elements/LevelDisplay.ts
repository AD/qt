export default class LevelDisplay extends Phaser.GameObjects.BitmapText {
  constructor(sc: Phaser.Scene) {
    super(sc, 205, 614, 'level', '', 26)
    this.setDepth(1)
    this.setOrigin(0.5)
    sc.add.existing(this)
    let ld = this
    sc.events.on('leveldisplay', function (score: number) {
      let str = score.toString()
      ld.setText(str)
    })
  }
}