import GameScene from '../scenes/GameScene';
import { ATLAS_1 } from '../setup/strings';

export default class Starburst extends Phaser.GameObjects.Particles.ParticleEmitterManager {

  public playG: (x: number, y: number) => void

  constructor(sc: GameScene) {
    super(sc, ATLAS_1, 'burst-star')
    this.depth = 250
    let stars = this.createEmitter({
      speed: { min: 100, max: 250 },
      scale: { start: 1, end: 0 },
      active: false,
      lifespan: 400,
      quantity: 25,
      rotate: { start: 0, end: 360, steps: 25 }
    });
    sc.add.existing(this)

    this.playG = (x, y) => {
      stars.active = true
      stars.explode(25, x, y)
    }
  }


}