import GameScene from '../scenes/GameScene'
import { ATLAS_1 } from '../setup/strings'

export default class extends Phaser.GameObjects.Mesh {

  constructor(sc: GameScene, x: number, y: number, frameKeys: string[], grid: number[][], _mirror?: []) {

    let mirrorExists = false
    if (_mirror !== undefined) {
      mirrorExists = true
    }

    let data: { [k: string]: any } = {}

    let len = frameKeys.length

    for (let i = 0; i < len; i++) {
      let frame = sc.textures.getFrame(ATLAS_1, frameKeys[i])
      let u = frame.u0
      let v = frame.v0
      let u1 = frame.u1
      let v1 = frame.v1

      data[frameKeys[i]] = {}

      if (mirrorExists) {
        // add horizontal flip
        data[frameKeys[i]].tuh = [
          u1, v,
          u1, v1,
          u, v1,
          u1, v,
          u, v1,
          u, v
        ]

        // add vertical flip
        data[frameKeys[i]].tuv = [
          u, v1,
          u, v,
          u1, v,
          u, v1,
          u1, v,
          u1, v1
        ]

        // add both 
        data[frameKeys[i]].tub = [
          u1, v1,
          u1, v,
          u, v,
          u1, v1,
          u, v,
          u, v1
        ]
      }

      data[frameKeys[i]].tu = [
        u, v,
        u, v1,
        u1, v1,
        u, v,
        u1, v1,
        u1, v
      ]
    }


    let refFrame = sc.textures.getFrame(ATLAS_1, frameKeys[0])
    let tileWid = refFrame.cutWidth
    let tileHgt = refFrame.cutHeight

    let numRows = grid.length
    let numColumns = grid[0].length

    let verts: number[] = []
    let uvs: number[] = []

    for (let row = 0; row < numRows; row++) {
      for (let col = 0; col < numColumns; col++) {
        if (grid[row][col] !== -1) {
          let x1 = col * tileWid
          let x2 = x1 + tileWid
          let y1 = row * tileHgt
          let y2 = y1 + tileHgt
          verts.push(x1)
          verts.push(y1)
          verts.push(x1)
          verts.push(y2)
          verts.push(x2)
          verts.push(y2)
          verts.push(x1)
          verts.push(y1)
          verts.push(x2)
          verts.push(y2)
          verts.push(x2)
          verts.push(y1)

          let frameKey = frameKeys[grid[row][col]]
          // let frame = sc.textures.getFrame(ATLAS_1, frameKey)

          if (mirrorExists) {

            if (_mirror[row][col] === 0) {
              uvs = uvs.concat(data[frameKey].tu)
            }

            if (_mirror[row][col] === 1) {
              uvs = uvs.concat(data[frameKey].tuh)
            }

            else if (_mirror[row][col] === 2) {

              uvs = uvs.concat(data[frameKey].tuv)
            }

            else if (_mirror[row][col] === 3) {
              uvs = uvs.concat(data[frameKey].tub)
            }

          }

          else {
            uvs = uvs.concat(data[frameKey].tu)
          }
        }
      }
    }

    super(sc, x, y, verts, uvs, [], [], ATLAS_1)
    this.depth = 1
    sc.add.existing(this)
    sc.addObj(this)
  }
}