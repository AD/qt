import GameScene from '../scenes/GameScene';
import { ATLAS_1 } from '../setup/strings';
import Button from "../ui elements/Button";

export default class extends Button {

  constructor(baseScene: Phaser.Scene, sc: GameScene) {
    super(baseScene, 1240, 40, ATLAS_1, 'menu')

    this.depth = 300
    this.enableBtn()

    this.on('pointerup', function () {
      this.resetBtn()
      sc.levelMenu()
    })

  }
}