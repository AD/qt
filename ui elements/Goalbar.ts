import GameScene from '../scenes/GameScene';
import { GOAL_HGT, GOAL_WID } from "../setup/const";
import { ATLAS_1 } from '../setup/strings';
import bounce from '../tweens/bounce';
import pulse from "../tweens/pulse";

export default class Goalbar {

  // Goalbar is just for display
  // all actual absortion checking happens in Detector

  public showAlert: () => void
  public hideAlert: () => void
  public updateGB: (per: number) => void
  public flag: Phaser.GameObjects.Image

  constructor(sc: GameScene, x: number, y: number, goal: number) {

    let base = sc.add.image(x, y, ATLAS_1, 'grad-base').setDepth(203)
    sc.addObj(base)
    let goalGrad = sc.add.image(x, y, ATLAS_1, 'goal-grad').setDepth(204)
    sc.addObj(goalGrad)
    goalGrad.setCrop(0, 0, 0, GOAL_HGT)

    let flag = ""
    if (goal === 1) {
      flag = "flag100"
    } else if (goal === .5) {
      flag = "flag50"
    } else if (goal === .25) {
      flag = "flag25"
    }

    this.flag = sc.add.image(x - 50 + Math.round(GOAL_WID * goal) - 1, y - 5, ATLAS_1, flag).setDepth(215).setOrigin(0, .5)
    sc.addObj(this.flag)
    let flagBounce = bounce(sc, this.flag)

    let alert = sc.add.image(x, y + 24, ATLAS_1, 'gnm').setDepth(215).setAlpha(0)
    sc.addObj(alert)
    let tween = pulse(sc, alert)

    this.showAlert = () => {
      tween.play()
      flagBounce.play()
    }
    this.hideAlert = () => {
      if (tween.isPlaying()) {
        tween.stop()
        flagBounce.stop()
        this.flag.y = y - 5
      }
      alert.alpha = 0
    }

    let curPer = 0
    let endPer = 0

    this.updateGB = (per) => {
      endPer = per
      sc.game.events.on('step', gradUpdate)
    }

    let gradUpdate = () => {
      if (Math.abs(curPer - endPer) > .001) {
        curPer += (endPer - curPer) / 10
        goalGrad.setCrop(0, 0, GOAL_WID * curPer, GOAL_HGT)
      } else {
        curPer = endPer
        goalGrad.setCrop(0, 0, GOAL_WID * endPer, GOAL_HGT)
        sc.game.events.off('step', gradUpdate)
      }
    }

  }

}