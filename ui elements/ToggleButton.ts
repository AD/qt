
export default class ToggleButton extends Phaser.GameObjects.Image {

  constructor(sc: Phaser.Scene, x: number, y: number, atlas: string, pre: string) {
    super(sc, x, y, atlas, pre + '_onu')

    const b = this
    let isOn = true

    // required frames 
    const onUpFrame = pre + '_onu'
    const onOverFrame = pre + '_ono'
    const onDownFrame = pre + '_ond'
    const offUpFrame = pre + '_offu'
    const offOverFrame = pre + '_offo'
    const offDownFrame = pre + '_offd'

    function setBehavior() {
      b.removeAllListeners()
      if (isOn) {
        b.on('pointerover', function () {
          b.setFrame(onOverFrame)
        })
        b.on('pointerout', function () {
          b.setFrame(onUpFrame)
        })
        b.on('pointerdown', function () {
          b.setFrame(onDownFrame)
        })
        b.on('pointerup', function () {
          b.setFrame(offUpFrame)
          isOn = false
          this.setOff()
          setBehavior()
        })
      } else {
        b.on('pointerover', function () {
          b.setFrame(offOverFrame)
        })
        b.on('pointerout', function () {
          b.setFrame(offUpFrame)
        })
        b.on('pointerdown', function () {
          b.setFrame(offDownFrame)
        })
        b.on('pointerup', function () {
          b.setFrame(onUpFrame)
          isOn = true
          this.setOn()
          setBehavior()
        })
      }
    }

    setBehavior()
    b.setInteractive()

    sc.add.existing(b)
  }

}
