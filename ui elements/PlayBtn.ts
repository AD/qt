import GameScene from '../scenes/GameScene';
import { ATLAS_1, START_SIM } from '../setup/strings';
import Button from "../ui elements/Button";

export default class extends Button {

  constructor(sc: GameScene, x: number, y: number, angle: number) {
    super(sc, x, y, ATLAS_1, 'play')

    this.setAngle(angle)
    this.setDepth(211)
    this.enableBtn()

    this.on('pointerup', function () {
      sc.events.emit(START_SIM)
    })

  }
}