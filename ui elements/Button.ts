
export default class Button extends Phaser.GameObjects.Image {

  public isEnabled: boolean = false
  public enableBtn: () => void
  public disableBtn: () => void
  public resetBtn: () => void

  constructor(sc: Phaser.Scene, x: number, y: number, atlas: string, pre: string) {
    super(sc, x, y, atlas, pre + '_u')

    let disabledSuffix: string = '_ds'
    if (!sc.textures.get(atlas).has(pre + disabledSuffix)) {
      disabledSuffix = '_u'
    }

    let b = this

    b.enableBtn = () => {
      if (!b.isEnabled) {
        b.on('pointerover', over)
        b.on('pointerout', out)
        b.on('pointerdown', down)
        b.on('pointerup', up)
        b.setInteractive({
          cursor: 'pointer'
        })
        b.setFrame(pre + '_u')
        b.isEnabled = true
      }
    }

    let over = () => {
      b.setFrame(pre + '_o')
    }

    let out = () => {
      b.setFrame(pre + '_u')
    }

    let down = () => {
      b.setFrame(pre + '_d')
    }

    let up = () => {
      b.setFrame(pre + '_o')
    }

    b.disableBtn = () => {
      b.removeAllListeners()
      b.disableInteractive()
      b.setFrame(pre + disabledSuffix)
      b.isEnabled = false
    }

    b.resetBtn = () => {
      out()
    }

    sc.add.existing(b)
  }
}