// Custom phaser game class
// adds in all custom game components

import LevelMgr from './game components/LevelMgr'
import Detection from './game components/Detection'
import Grad from './game components/Grad'
import Grid from './game components/Grid'
import InputControl from './game components/InputControl'
import Paths from './game components/Paths'
import GameState from './game components/GameState'

export default class GameC extends Phaser.Game {

  public levelMgr: LevelMgr
  public detection: Detection
  public grad: Grad
  public grid: Grid
  public inputControl: InputControl
  public paths: Paths
  public gameState: GameState

  public goFullScreen: boolean = false

  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config)
    const g = this
    g.levelMgr = new LevelMgr()
    g.detection = new Detection()
    g.grad = new Grad()
    g.grid = new Grid()
    g.inputControl = new InputControl(g)
    g.paths = new Paths()
    g.gameState = new GameState()
  }
}