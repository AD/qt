// NOTE this is only for mirror and splitter 
// polarsplitter behaves differently 
// ang can be 0 - 3 which corresponds to → ↘ ↓ ↙
// dir can be 0-3 which corresponds to     [1, 0], [0, 1], [-1, 0], [0, -1]

export default function (ang: number, dir: number) {

  // reflectAng is normalized to dir 0 
  // then changeDir works no matter what the input direction 

  let reflectAng = ang
  if (dir === 1) {
    reflectAng = normalAng(ang - 2)
  } else if (dir === 3) {
    reflectAng = normalAng(ang + 2)
  }

  return reflectAng

}

function normalAng(n: number) {
  if (n < 0) {
    n += 4
  } else if (n > 3) {
    n -= 4
  }
  return Math.round(n) // to correct browser rounding errors
}


// → ↘ ↓ ↙ ← ↖ ↑ ↗    
