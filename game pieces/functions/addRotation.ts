import GamePiece from "../GamePiece";
import rotateTo from "../../tweens/rotateTo";
import GameScene from '../../scenes/GameScene';
import { ATLAS_1, CLEAR_TRACERS, TRACE } from '../../setup/strings';

export default function (sc: GameScene, gp: GamePiece) {

  gp.rotIco = sc.add.image(0, 0, ATLAS_1, 'rot-ico').setDepth(215).setOrigin(1, 0)
  sc.addObj(gp.rotIco)
  gp.rotIcoExists = true
  gp.showIcons()

  gp.nextAng = function () {
    let newAng = gp.ang + gp.rotateInc
    if (newAng < 0) {
      newAng += gp.rotateLoop
    }
    gp.ang = newAng
  }

  gp.rotateG = function () {
    sc.input.enabled = false
    sc.events.emit(CLEAR_TRACERS)
    gp.hideAbsorb()

    if (!gp.rotateAnim) {
      let diff = '+=' + this.rotateInc * 45
      rotateTo(sc, gp, diff)
      gp.nextAng()
    }

    else {
      if (!gp.anims.isPlaying) {
        gp.nextAng()
        gp.once('animationcomplete', function () {
          gp.showIcons()
          sc.events.emit(TRACE)
        })
        gp.anims.play(gp.rotateAnims[gp.ang])
      }
    }

    // gp.playRotateSound()

  }

  gp.rotateOnly = function () {
    gp.on('pointerup', function () {
      gp.rotateG()
    })
  }
} 