export function addPhase(old: number, delta: number) {
  let p = old + delta
  if (p >= 1) {
    p -= 1
  }
  return p
}

export function subtractPhase(old: number, delta: number) {
  let p = old - delta
  if (p < 0) { p += 1 }
  return p
}