// POLAR ROTATE

import { INext } from "../../setup/types";
import { PI_14 } from "../../setup/const";
import { addPhase } from "./phase";
import getPer from "./getPer";

// dir 1 = CCW and `-1 = CW 
export default function (data: INext, dir: number) {
  if (data.hp >= .5) {
    data.hp -= .5
    data.ha = -data.ha
  }
  if (data.vp >= .5) {
    data.vp -= .5
    data.va = -data.va
  }

  // sets base p 
  if (data.ha === 0) {
    data.hp = data.vp
  }
  if (data.va === 0) {
    data.vp = data.hp
  }
  let newPoint = Phaser.Math.Rotate({
    x: data.ha,
    y: data.va
  }, dir * PI_14)

  data.ha = newPoint.x
  data.va = newPoint.y
  if (Phaser.Math.RoundTo(data.ha, -5) === 0) {
    data.ha = 0
    data.hp = 0
  } else {
    if (data.ha < 0) {
      data.ha = -data.ha
      data.hp = addPhase(data.hp, .5)
    }
  }
  if (Phaser.Math.RoundTo(data.va, -5) === 0) {
    data.va = 0
    data.vp = 0
  } else {
    if (data.va < 0) {
      data.va = -data.va
      data.vp = addPhase(data.vp, .5)
    }
  }
  data.per = getPer(data.ha, data.va)

  return data

}