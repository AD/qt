import GamePiece from "../GamePiece";
import { START_ANGLES } from "../../setup/const";
import GameScene from '../../scenes/GameScene';
import { ATLAS_1 } from "../../setup/strings";

export default function (sc: GameScene, gp: GamePiece) {

  let layer: Phaser.GameObjects.Sprite
  let layerExists = false
  let animName: string

  let bm = sc.add.bitmapText(0, 0, 'percent', '0.0%').setDepth(203).setAlpha(0).setOrigin(0.5)
  sc.addObj(bm)

  // this could be undefined for animated ones
  gp.addLayer = (pre: string) => {
    layer = sc.add.sprite(0, 0, ATLAS_1, pre + '.0000').setDepth(205)
    animName = pre
    layerExists = true
    sc.addObj(layer)
  }

  gp.absorb = () => {
    if (!sc.gameState.gameMode) {
      gp.showPercent()
    }
    else if (layerExists && sc.levelMgr.isTraceOn()) {
      layer.angle = START_ANGLES[gp.ang]
      if (Math.round(gp.per * 1000) / 1000 > 0) {
        layer.setPosition(gp.x, gp.y).setAlpha(1).setTint(sc.grad.getTintG(gp.per))
        layer.anims.play(animName)
      }
      else {
        layer.setAlpha
      }
    }
  }

  gp.showPercent = () => {
    let str = (gp.per * 100).toFixed(1) + '%'
    bm.setPosition(Math.round(gp.x), Math.round(gp.y) - 45).setText(str).setAlpha(1).setTint(sc.grad.getTintG(gp.per))
  }

  gp.hideAbsorb = () => {
    gp.per = 0
    if (layerExists) {
      layer.clearTint().setAlpha(0)
      if (layer.anims !== undefined) {
        layer.anims.stop()
      }
    }
  }

  gp.hidePercent = () => {
    bm.clearTint().setAlpha(0)
  }

}