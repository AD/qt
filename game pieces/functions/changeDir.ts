export default function (dir: number, reflectAng: number) {

  if (reflectAng === 1) {
    dir++
    if (dir > 3) { dir = 0 }
  }

  else if (reflectAng === 2) {
    dir += 2
    if (dir > 3) { dir -= 4 }
  }

  else if (reflectAng === 3) {
    dir--
    if (dir < 0) { dir = 3 }
  }

  return dir
}