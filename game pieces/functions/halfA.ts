export default function (a: number) {
  return Math.sqrt(a * a / 2)
}