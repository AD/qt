import GamePiece from "../GamePiece";
import hiliteFlash from "../../tweens/hiliteFlash";
import moveTo from "../../tweens/moveTo"
import GameScene from '../../scenes/GameScene';
import { CELL_SIZE } from '../../setup/const';
import { ATLAS_1, STOP_SIM, TRACE } from '../../setup/strings';

export default function (sc: GameScene, gp: GamePiece) {

  let snap = Phaser.Math.Snap.To

  let lastGridX = snap(gp.x - sc.grid.getCellX(0), CELL_SIZE, 0, true)
  let lastGridY = snap(gp.y - sc.grid.getCellY(0), CELL_SIZE, 0, true)
  let newGridX = 0
  let newGridY = 0
  let startDragX = 0
  let startDragY = 0

  gp.moveIco = sc.add.image(0, 0, ATLAS_1, 'move-ico').setDepth(215).setOrigin(0)
  sc.addObj(gp.moveIco)
  gp.moveIcoExists = true
  gp.showIcons()

  gp.on('dragstart', function () {
    gp.hideAbsorb()
    gp.hideIcons()
    gp.setDepth(212)
    startDragX = gp.x
    startDragY = gp.y
    sc.grid.setObjOnGrid(lastGridY, lastGridX, null)
    sc.events.emit(STOP_SIM)
  })

  gp.on('drag', function (p: Phaser.Input.Pointer, x: number, y: number) {
    gp.x = x
    gp.y = y
    sc.cornerFrame.setPosition(x, y)

    newGridX = snap(gp.x - sc.grid.getCellX(0), CELL_SIZE, 0, true)
    newGridY = snap(gp.y - sc.grid.getCellY(0), CELL_SIZE, 0, true)

    if (
      newGridX >= 0 &&
      newGridY >= 0 &&
      newGridX < sc.grid.getNumCols() &&
      newGridY < sc.grid.getNumRows()
    ) {
      if (sc.grid.getObjOnGrid(newGridY, newGridX) === null) {
        if (lastGridX !== newGridX || lastGridY !== newGridY) {
          lastGridX = newGridX
          lastGridY = newGridY
          sc.hilite.setPosition(sc.grid.getCellX(lastGridX), sc.grid.getCellY(lastGridY))
          hiliteFlash(sc)
        }
      }
    }
  })

  gp.on('dragend', function () {
    sc.hilite.alpha = 0
    gp.setDepth(210)

    sc.grid.setObjOnGrid(lastGridY, lastGridX, gp)

    if (Math.abs(startDragX - gp.x) < 8 && Math.abs(startDragY - gp.y) < 8) {
      gp.x = startDragX
      gp.y = startDragY
      if (gp.rotates) {
        gp.rotateG()
      } else {
        gp.showIcons()
        sc.events.emit(TRACE)
      }

    }
    else {
      moveTo(sc, gp, sc.grid.getCellX(lastGridX), sc.grid.getCellY(lastGridY))
      // gp.playDropSound()
    }

    sc.cornerFrame.setPosition(sc.grid.getCellX(lastGridX), sc.grid.getCellY(lastGridY))

  })


  sc.input.setDraggable(gp)

}
