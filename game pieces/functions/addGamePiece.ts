import GameScene from '../../scenes/GameScene';
import { ABSORBER, FARADAY, GLASS, MINE, MIRROR, POLARIZER, POLAR_SPLITTER, ROCK, SPLITTER, SUGAR } from '../../setup/strings';
import Absorber from "../types/Absorber";
import Faraday from "../types/Faraday";
import Glass from "../types/Glass";
import Mine from "../types/Mine";
import Mirror from "../types/Mirror";
import Polarizer from "../types/Polarizer";
import PolarSplitter from "../types/PolarSplitter";
import Rock from "../types/Rock";
import Splitter from "../types/Splitter";
import Sugar from "../types/Sugar";

export default function (sc: GameScene, type: string) {
  let obj = createObj(type)
  sc.allGamePieces.add(obj)
  return obj

  function createObj(type: string) {
    switch (type) {
      case ABSORBER:
        return new Absorber(sc)
      case FARADAY:
        return new Faraday(sc)
      case GLASS:
        return new Glass(sc)
      case MINE:
        return new Mine(sc)
      case MIRROR:
        return new Mirror(sc)
      case POLARIZER:
        return new Polarizer(sc)
      case POLAR_SPLITTER:
        return new PolarSplitter(sc)
      case ROCK:
        return new Rock(sc)
      case SPLITTER:
        return new Splitter(sc)
      case SUGAR:
        return new Sugar(sc)
      default:
      // throw new Error('object type name error:' + type)
    }
  }
}