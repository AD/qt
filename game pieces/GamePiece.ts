import GameScene from '../scenes/GameScene'
import { START_ANGLES } from '../setup/const'
import { ATLAS_1 } from '../setup/strings'
import { INext } from '../setup/types'

export default class GamePiece extends Phaser.Physics.Matter.Sprite {

  // properties accessed here 
  public rType: string = ''
  public ang: number = 0
  public rotates: boolean = false
  public interact: () => void
  public hidePercent: () => void
  public hideAbsorb = () => { }
  public rotateG: () => void
  public setAng: (n: number) => void
  public showIcons: () => void
  public hideIcons: () => void
  public playDropSound: () => void
  public dropSoundKey: string = 'drop'
  public playRotateSound: () => void

  // properties used in child classes
  public moveIcoExists: boolean = false
  public rotIcoExists: boolean = false
  public moveIco: Phaser.GameObjects.Image
  public rotIco: Phaser.GameObjects.Image
  public per: number = 0
  public isEndpoint: boolean = false
  public isSplitter: boolean = false
  public rotateAnim: boolean = false
  public rotateAnims: string[] = []
  public rotateInc: number = 0
  public rotateLoop: number = 0
  public showPercent: () => void
  public absorb: () => void
  public nextAng: () => void
  public process: (data: INext) => INext[]
  public addLayer: (pre: string) => void
  public rotateOnly: () => void
  public setShape: () => void
  // public updatePosition: () => void
  public playStarburst: () => void

  constructor(sc: GameScene) {

    super(sc.matter.world, 0, 0, ATLAS_1)

    let gp = this
    sc.matter.world.remove(gp)

    gp.setDepth(210)

    gp.setAng = function (n) {
      gp.ang = n
      if (!gp.rotateAnim) {
        gp.angle = START_ANGLES[gp.ang]
      } else {
        gp.setFrame(
          sc.anims.get(gp.rotateAnims[gp.ang]).getLastFrame().textureFrame
        )
      }
    }

    gp.showIcons = function () {
      if (gp.moveIcoExists) {
        gp.moveIco.setPosition(gp.x - 48, gp.y - 48)
        gp.moveIco.setAlpha(1)
      }
      if (gp.rotIcoExists) {
        gp.rotIco.setPosition(gp.x + 50, gp.y - 48)
        gp.rotIco.setAlpha(1)
      }
    }

    gp.hideIcons = function () {
      if (gp.moveIcoExists) {
        gp.moveIco.setAlpha(0)
      }
      if (gp.rotIcoExists) {
        gp.rotIco.setAlpha(0)
      }
    }

    gp.interact = function () {
      gp.setInteractive({
        cursor: 'pointer'
      })
      gp.on('pointerover', function () {
        sc.cornerFrame.setPosition(this.x, this.y).setAlpha(1)
      })
      gp.on('pointerout', function () {
        sc.cornerFrame.setAlpha(0)
      })
    }

    // gp.playDropSound = function () {
    //   sc.sound.play(gp.dropSoundKey, {
    //     volume: 0.5,
    //   })
    // }

    // gp.playRotateSound = function () {
    //   sc.sound.play('rotate', {
    //     volume: 0.5,
    //   })
    // }

  }

}