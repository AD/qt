import GamePiece from "../GamePiece"
import { INext } from "../../setup/types"
import getPer from "../functions/getPer"
import halfA from "../functions/halfA"
import addAbsorption from "../functions/addAbsorption"
import GameScene from '../../scenes/GameScene'
import { ABSORBER } from '../../setup/strings'

export default class Absorber extends GamePiece {

  public rType: string = ABSORBER

  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    this.setFrame("absorber")
    this.addLayer('absl')

    sc.add.existing(this)
  }

  public process = function (pData: INext) {
    pData.ha = halfA(pData.ha)
    pData.va = halfA(pData.va)
    pData.per = getPer(pData.ha, pData.va)
    this.per += pData.per
    this.absorb()
    return [pData]
  }

  public setShape = function () {
    this.hideAbsorb()
    this.setRectangle(this.width, this.height)

  }

}