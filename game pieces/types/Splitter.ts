import GamePiece from "../GamePiece"
import { INext } from "../../setup/types"
import getReflectAng from "../functions/getReflectAng"
import changeDir from "../functions/changeDir"
import { addPhase, subtractPhase } from "../functions/phase"
import halfA from "../functions/halfA"
import getPer from "../functions/getPer"
import addAbsorption from "../functions/addAbsorption"
import GameScene from '../../scenes/GameScene'
import { SPLITTER } from '../../setup/strings'


export default class Splitter extends GamePiece {

  public rType: string = SPLITTER
  public rotateAnims: string[] = ['sp0', 'sp1', 'sp2', 'sp3']
  public rotateInc: number = -1
  public rotateLoop: number = 4

  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    this.isSplitter = true
    this.rotates = true
    this.rotateAnim = true

    this.setFrame("spl.0000")
    this.addLayer('mirl')

    sc.add.existing(this)
  }

  public process = function (input: INext) {

    let reflectAng = getReflectAng(this.ang, input.dir)

    if (reflectAng === 0) {
      this.per += input.per
      this.absorb()
      input.dir = -1
      return [input]
    }

    // straight out 
    let out1: INext = Object.assign({}, input)

    out1.ha = halfA(input.ha)
    out1.va = halfA(input.va)
    out1.per = getPer(out1.ha, out1.va)

    // reflected out
    let out2: INext = Object.assign({}, input)
    out2.dir = changeDir(input.dir, reflectAng)

    out2.ha = halfA(input.ha)
    out2.va = halfA(input.va)
    out2.per = getPer(out2.ha, out2.va)

    if (Phaser.Math.RoundTo(out2.ha, -5) > 0) {
      out2.hp = addPhase(input.hp, .25)

    }
    if (Phaser.Math.RoundTo(out2.va, -5) > 0) {
      out2.vp = subtractPhase(input.vp, .25)

    }

    return [out1, out2]

  }

  public setShape = function () {
    this.hideAbsorb()
    this.setRectangle(this.width, this.height)
  }

}