import GamePiece from "../GamePiece"
import { INext } from "../../setup/types"
import getReflectAng from "../functions/getReflectAng"
import changeDir from "../functions/changeDir"
import { addPhase } from "../functions/phase"
import getPer from "../functions/getPer"
import addAbsorption from "../functions/addAbsorption"
import GameScene from '../../scenes/GameScene'
import { MIRROR } from '../../setup/strings'

export default class Mirror extends GamePiece {

  public rType: string = MIRROR

  public rotateAnims: string[] = ['m0', 'm1', 'm2', 'm3']
  public rotateInc: number = -1
  public rotateLoop: number = 4


  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    this.rotates = true
    this.rotateAnim = true

    this.setFrame("mir.0000")
    this.addLayer('mirl')

    sc.add.existing(this)
  }

  public process = function (input: INext) {

    let reflectAng = getReflectAng(this.ang, input.dir)

    if (reflectAng === 0) {
      this.per += getPer(input.ha, input.va)
      this.absorb()
      input.dir = -1
      return [input]
    }

    input.dir = changeDir(input.dir, reflectAng)

    if (Math.abs(input.va) > 0) {
      input.vp = addPhase(input.vp, .5)
    }

    return [input]

  }

  public setShape = function () {
    this.hideAbsorb()
    this.setRectangle(this.width, this.height)
  }

}