import GameScene from '../../scenes/GameScene'
import { GLASS } from '../../setup/strings'
import {
  INext
} from "../../setup/types"
import { addPhase } from "../functions/phase"
import GamePiece from "../GamePiece"

export default class Glass extends GamePiece {

  public rType: string = GLASS

  constructor(sc: GameScene) {
    super(sc)

    this.setFrame("glass")

    sc.add.existing(this)
  }

  public process = function (pData: INext) {
    if (Math.abs(pData.ha) > 0) {
      pData.hp = addPhase(pData.hp, .25)
    }
    if (Math.abs(pData.va) > 0) {
      pData.vp = addPhase(pData.vp, .25)
    }
    return [pData]
  }

  public setShape = function () {
    this.setRectangle(this.width, this.height)
  }

}