
import { START_ANGLES } from "../../setup/const"
import Goalbar from "../../ui elements/Goalbar"
import GameScene from '../../scenes/GameScene'
import { ATLAS_1, DETECTOR } from '../../setup/strings'

export default class Detector extends Phaser.Physics.Matter.Sprite {

  public rType: string = DETECTOR
  public layer: Phaser.GameObjects.Sprite
  public playStarburst: () => void
  public hideAbsorb = () => { }
  public absorb: () => void
  public per: number = 0
  public reached: () => boolean
  public ang: number = 0

  public showAlert: () => void
  public hideAlert: () => void

  // TODO adding dummy functions here since it is called in trace
  public process: () => void

  private goal: number

  constructor(sc: GameScene, x: number, y: number, ang: number, goal: number, gradDir: number, showGrad: boolean) {
    super(sc.matter.world, x, y, ATLAS_1, 'plant.0000')

    let d = this
    sc.matter.world.remove(d)

    d.displayOriginX += 23
    d.goal = goal

    d.setDepth(210)
    d.angle = START_ANGLES[ang]
    d.ang = ang

    function plantAnim() {
      let delay = Phaser.Math.RND.between(2000, 4000)
      d.anims.delayedPlay(delay, 'p1')
    }
    d.on("animationcomplete", plantAnim)
    plantAnim()

    let gX = d.x - 24
    let gY = d.y
    if (gradDir === 1) {
      gY += 62
      if (ang === 4) {
        gX = d.x + 24
      }
    }
    if (gradDir === 3) {
      gY -= 54
      if (ang === 4) {
        gX = d.x + 24
      }
    }
    else if (gradDir === 0) {
      gX += 114
    }
    else if (gradDir === 2) {
      gX -= 72
    }

    var goalBar = new Goalbar(sc, gX, gY, goal)

    d.reached = function () {
      if (Math.abs(d.goal - d.per) < .001) {
        return true
      } else {
        return false
      }
    }

    d.playStarburst = function () {
      if (d.reached()) {
        sc.playStarburst(d.x, d.y)
      }
    }

    d.absorb = function () {
      if (sc.gameState.gameMode) {
        if (showGrad) {
          if (d.per === 0) {
            goalBar.updateGB(0)
          } else {
            let tint = sc.grad.getTintG(d.per)
            goalBar.updateGB(d.per)
          }
        } else {
          goalBar.updateGB(0)
        }
      } else {
        // TODO update percent 
      }
    }

    d.showAlert = function () {
      goalBar.updateGB(d.per)
      if (!d.reached()) {
        goalBar.showAlert()
      }
    }

    d.hideAlert = function () {
      goalBar.hideAlert()
    }

    d.hideAbsorb = function () {
      if (showGrad) {

      }
    }



    sc.add.existing(d)

  }

  public setShape() {
    this.off('animationcomplete')
    this.setCircle(this.width / 2)
  }

}



