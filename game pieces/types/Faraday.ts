import GameScene from '../../scenes/GameScene'
import { FARADAY } from '../../setup/strings'
import { INext } from "../../setup/types"
import addAbsorption from "../functions/addAbsorption"
import getPer from "../functions/getPer"
import polRot from "../functions/polRot"
import GamePiece from "../GamePiece"


export default class Faraday extends GamePiece {

  public rType: string = FARADAY

  public rotateInc: number = -2
  public rotateLoop: number = 8

  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    this.rotates = true

    this.setFrame("faraday")
    this.addLayer('farl')

    sc.add.existing(this)

    this.process = function (pData) {

      // absorbs if perpendicular

      // if dir 0,2 and ang 2,6 (90,270) - absorb
      // if dir 1,3 and ang 0,4 (0, 180) - absorb 
      if (pData.dir === 0 || pData.dir === 2) {
        if (this.ang === 2 || this.ang === 6) {
          this.per += getPer(pData.ha, pData.va)
          this.absorb()
          pData.dir = -1
          return [pData]
        }
      }
      if (pData.dir === 1 || pData.dir === 3) {
        if (this.ang === 0 || this.ang === 4) {
          this.per += getPer(pData.ha, pData.va)
          this.absorb()
          pData.dir = -1
          return [pData]
        }
      }

      let dir = -1 // enter from right
      if (pData.dir === this.ang / 2) { // enter from left
        dir = 1
      }

      return [polRot(pData, dir)]
    }
  }


  public setShape = function () {
    this.hideAbsorb()
    this.setRectangle(this.width, this.height)
  }

}