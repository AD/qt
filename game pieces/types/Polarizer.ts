import GameScene from '../../scenes/GameScene'
import { PI_14 } from "../../setup/const"
import { POLARIZER } from '../../setup/strings'
import { INext } from "../../setup/types"
import addAbsorption from "../functions/addAbsorption"
import halfA from "../functions/halfA"
import GamePiece from "../GamePiece"


export default class Polarizer extends GamePiece {

  public rType: string = POLARIZER
  public rotateInc: number = -1
  public rotateLoop: number = 4

  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    this.rotates = true

    this.setFrame("polar")
    this.addLayer('poll')

    sc.add.existing(this)
  }

  public process = function (pData: INext) {

    // set ang to 0-3, 4-7 are identical
    // corresponds to 0, 45, 90, 135
    if (this.ang > 3) {
      this.ang -= 4
    }

    // normAng is same as ang if dir = 0
    let normAng = this.ang + pData.dir * 2
    if (normAng > 3) {
      normAng -= 4
    }

    if (normAng === 0) {
      this.per += pData.va * pData.va
      pData.per = pData.ha * pData.ha
      pData.va = 0
      pData.vp = 0
    }
    if (normAng === 2) {
      this.per += pData.ha * pData.ha
      pData.per = pData.va * pData.va
      pData.ha = 0
      pData.hp = 0
    }

    if (normAng === 1 || normAng === 3) {
      let newPoint
      // let testHp = pData.hp
      // let testVp = pData.vp
      let testHa = pData.ha
      let testVa = pData.va

      if (pData.hp >= .5) {
        // testHp -= .5
        testHa = -testHa
      }
      if (pData.vp >= .5) {
        // testVp -= .5
        testVa = -testVa
      }

      // now it is a 50/50 split 
      if (normAng === 3) {
        // rotating CW 
        newPoint = Phaser.Math.Rotate({
          x: testHa,
          y: testVa
        }, -PI_14)
        if (newPoint.x < 0) {
          pData.hp = .5
          pData.vp = .5
        } else {
          pData.hp = 0
          pData.vp = 0
        }
      } else {
        // rotating CCW
        newPoint = Phaser.Math.Rotate({
          x: testHa,
          y: testVa
        }, PI_14)
        if (newPoint.x < 0) {
          pData.hp = .5
          pData.vp = 0
        } else {
          pData.hp = 0
          pData.vp = .5
        }
      }

      pData.ha = halfA(newPoint.x)
      pData.va = pData.ha

      pData.per = newPoint.x * newPoint.x
      this.per += newPoint.y * newPoint.y

    }

    if (this.per > 0) {
      this.absorb()
    }
    return [pData]
  }

  public setShape = function () {
    this.hideAbsorb()
    this.setCircle(this.width / 2)
  }
}