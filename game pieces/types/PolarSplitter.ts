import GamePiece from "../GamePiece"
import { INext } from "../../setup/types"
import GameScene from '../../scenes/GameScene'
import { POLAR_SPLITTER } from '../../setup/strings'


export default class PolarSplitter extends GamePiece {

  public rType: string = POLAR_SPLITTER

  public rotateAnims: string[] = ['ps0', '', 'ps1', '', 'ps2', '', 'ps3', '']
  public rotateInc: number = -2
  public rotateLoop: number = 8

  constructor(sc: GameScene) {
    super(sc)

    this.isSplitter = true
    this.rotates = true
    this.rotateAnim = true

    this.setFrame("ps.0000")

    sc.add.existing(this)
  }

  public process = function (input: INext) {

    let out1: INext = Object.assign({}, input)
    let out2: INext = Object.assign({}, input)

    // reflects vertical 90/270
    // transmits horizontal 0/180
    // both if 45s

    // get 0 or 1 as test Ang 
    let testAng = this.ang / 2
    if (testAng > 1) {
      testAng -= 2
    }
    if (input.dir === 0 || input.dir === 2) {
      if (testAng === 0) {
        out2.dir--
        if (out2.dir < 0) {
          out2.dir += 4
        }
      } else {
        out2.dir++
        if (out2.dir > 3) {
          out2.dir -= 4
        }
      }
    } else {
      if (testAng === 1) {
        out2.dir--
        if (out2.dir < 0) {
          out2.dir += 4
        }
      } else {
        out2.dir++
        if (out2.dir > 3) {
          out2.dir -= 4
        }
      }
    }

    out1.va = 0 // leave only ha on pass through
    out1.vp = 0
    out2.ha = 0 // leave only va on reflect 
    out2.hp = 0
    out1.per = out1.ha * out1.ha
    out2.per = out2.va * out2.va
    return [out1, out2]

  }

  public setShape = function () {
    this.setRectangle(50, 50)
  }

}