import GameScene from '../../scenes/GameScene'
import { ATLAS_1, ROCK } from '../../setup/strings'
import { INext } from "../../setup/types"
import addAbsorption from "../functions/addAbsorption"
import GamePiece from "../GamePiece"


export default class Rock extends GamePiece {

  public rType: string = ROCK
  public preDestroy = function () { }

  constructor(sc: GameScene) {
    super(sc)

    addAbsorption(sc, this)

    let r = this

    r.setFrame("rock-base")
    r.addLayer('rockl')

    r.dropSoundKey = 'drop2'

    let eyes = sc.add.sprite(0, 0, ATLAS_1, 'rockeyes.0000').setDepth(6)
    eyes.displayOriginX = 18
    eyes.displayOriginY = 13
    sc.addObj(eyes)

    let curAnim = 0

    function rockAnim() {
      let delay = Phaser.Math.RND.between(1000, 2000)
      eyes.anims.delayedPlay(delay, 'r' + curAnim)
      let change = Phaser.Math.RND.between(1, 10)
      if (Phaser.Math.IsEven(curAnim)) {
        if (change > 7) {
          curAnim++
          if (curAnim === 10) {
            curAnim = 0
          }
        }
      } else {
        curAnim++
        if (curAnim === 10) {
          curAnim = 0
        }
      }
    }
    eyes.on("animationcomplete", rockAnim)
    rockAnim()

    r.preUpdate = function (time, delta) {
      if (eyes.depth !== r.depth + 1) {
        eyes.depth = r.depth + 1
      }
      eyes.x = r.x // - 18
      eyes.y = r.y //  - 13
      eyes.scaleX = r.scaleX
      eyes.scaleY = r.scaleY
      eyes.angle = r.angle
      eyes.alpha = r.alpha
    }

    r.preDestroy = function () {
      eyes.destroy()
    }

    sc.add.existing(r)
  }

  public process = function (input: INext) {
    this.per += input.per
    this.absorb()
    input.dir = -1
    return [input]
  }

  public setShape = function () {
    this.hideAbsorb()
    this.setRectangle(this.width, this.height)
  }

}