import GamePiece from "../GamePiece"
import {
  INext
} from "../../setup/types"
import polRot from "../functions/polRot"
import GameScene from '../../scenes/GameScene'
import { SUGAR } from '../../setup/strings'


export default class Sugar extends GamePiece {

  public rType: string = SUGAR

  constructor(sc: GameScene) {
    super(sc)

    this.setFrame("sugar")

    sc.add.existing(this)
  }

  public process = function (pData: INext) {
    return [polRot(pData, 1)]
  }

  public setShape = function () {
    this.setRectangle(this.width, this.height)
  }

}