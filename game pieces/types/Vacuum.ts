import GamePiece from "../GamePiece"
import {
  INext
} from "../../setup/types"
import { subtractPhase } from "../functions/phase"
import GameScene from '../../scenes/GameScene'


export default class Vacuum extends GamePiece {

  public rType: string = 'vacuum'

  constructor(sc: GameScene) {
    super(sc)

    this.setFrame("vacuum")

    sc.add.existing(this)
  }

  public process = function (pData: INext) {
    if (Math.abs(pData.ha) > 0) {
      pData.hp = subtractPhase(pData.hp, .25)
    }
    if (Math.abs(pData.va) > 0) {
      pData.vp = subtractPhase(pData.vp, .25)
    }
    return [pData]
  }

  public setShape = function () {
    this.setRectangle(this.width, this.height)
  }
}