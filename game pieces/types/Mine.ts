import GameScene from '../../scenes/GameScene'
import { MINE } from '../../setup/strings'
import GamePiece from "../GamePiece"

export default class Mine extends GamePiece {

  public rType: string = MINE

  constructor(sc: GameScene) {
    super(sc)

    this.isEndpoint = true

    this.setFrame("mine")

    sc.add.existing(this)
  }

  public setShape = function () {
    this.setCircle(this.width / 2)
  }

}