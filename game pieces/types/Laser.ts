import { START_ANGLES } from "../../setup/const"
import GameScene from '../../scenes/GameScene'
import PlayBtn from '../../ui elements/PlayBtn'
import { ATLAS_1, LASER } from '../../setup/strings'

export default class Laser extends Phaser.Physics.Matter.Image {

  public rType: string = LASER

  public hideAbsorb = () => { }

  public showPercent: () => void

  // NOTE adding dummy functions here since it is called in trace
  public process: () => void

  constructor(sc: GameScene, x: number, y: number, ang: number) {
    super(sc.matter.world, x, y, ATLAS_1, 'laser')

    let l = this

    l.setFrame("laser")

    l.setDisplayOrigin(190, 40)

    sc.matter.world.remove(l)

    l.setDepth(5)

    l.angle = START_ANGLES[ang]

    sc.add.existing(l)

    let xDif = 0
    let yDif = 0
    if (ang === 0) {
      xDif = -97
    }
    if (ang === 4) {
      xDif = 97
    }
    if (ang === 2) {
      yDif = -97
    }
    if (ang === 6) {
      yDif = 97
    }

    if (sc.gameState.gameMode) {
      let playBtn = new PlayBtn(sc, l.x + xDif, l.y + yDif, START_ANGLES[ang])
      sc.addObj(playBtn)
    }

  }

  public setShape() {
    this.setRectangle(this.width, this.height)
  }
}