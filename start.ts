import 'phaser';
import GameC from './GameC';
import BackScene from './scenes/BackScene';
import BootScene from './scenes/BootScene';
import GameScene from './scenes/GameScene';
import LoadScene from './scenes/LoadScene';
import MenuScene from './scenes/MenuScene';
import ModalScene from './scenes/modals/ModalScene';
import { GAME_WIDTH, GAME_HEIGHT } from './setup/const';

let scaleConfig: Phaser.Types.Core.ScaleConfig = {
  mode: Phaser.Scale.FIT,
  autoCenter: Phaser.Scale.CENTER_BOTH,
  width: GAME_WIDTH,
  height: GAME_HEIGHT,
  max: {
    width: GAME_WIDTH,
    height: GAME_HEIGHT
  }
}

let config = {
  type: Phaser.WEBGL,
  scale: scaleConfig,
  physics: {
    default: 'matter',
  },
  render: {
    clearBeforeRender: false
  },
  dom: {
    createContainer: true
  },
  scene: [
    BootScene,
    LoadScene,
    BackScene,
    GameScene,
    MenuScene,
    ModalScene,
  ],
  parent: 'game'
};

new GameC(config)

