// events
export const SET_BACK = '#sb'
export const SETUP_LEVEL = '#sl'
export const GAME_MODE = '#gm'
export const STATE_MODE = '#sm'
export const END_STEP = '#es'
export const CLEAR_TRACERS = '#ct'
export const START_SIM = '#st'
export const STOP_SIM = '#ss'
export const TRACE = '#t'

// scene keys
export const BOOT_SCENE = 'b'
export const LOAD_SCENE = 'l'
export const BACK_SCENE = 'k'
export const MENU_SCENE = 'm'
export const GAME_SCENE = 'g'
export const MODAL_SCENE = 'd'
export const NONE = 'n'

// gamepiece types 
export const ABSORBER = '_a'
export const FARADAY = '_f'
export const GLASS = '_g'
export const MINE = '_m'
export const MIRROR = '_mi'
export const POLARIZER = '_p'
export const POLAR_SPLITTER = '_ps'
export const ROCK = '_r'
export const SPLITTER = '_sp'
export const SUGAR = '_s'

export const LASER = '_l'
export const DETECTOR = '_d'

export const EMPTY = '_e'

export const ATLAS_1 = 'a1$'
export const TITLE_ATLAS = 'a2$'
export const MODAL_ATLAS = 'a3$'

export const CURRENT_UNLOCKED_LEVEL = 'cl'