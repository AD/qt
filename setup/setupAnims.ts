import { ATLAS_1 } from "./strings"

export default function setupAnims(sc: Phaser.Scene) {

  // prefix, animKey, startFrame, numFrames, fps, repeat
  let animData: [string, string, number, number, number, number][] = [
    ['wh0', 'wh0', 0, 14, 30, -1],
    ['wh1', 'wh1', 0, 14, 30, -1],
    ['wh2', 'wh2', 0, 14, 30, -1],
    ['wh3', 'wh3', 0, 14, 30, -1],
    ['yl0', 'yl0', 0, 14, 30, -1],
    ['yl1', 'yl1', 0, 14, 30, -1],
    ['yl2', 'yl2', 0, 14, 30, -1],
    ['yl3', 'yl3', 0, 14, 30, -1],
    ['rd0', 'rd0', 0, 14, 30, -1],
    ['rd1', 'rd1', 0, 14, 30, -1],
    ['rd2', 'rd2', 0, 14, 30, -1],
    ['rd3', 'rd3', 0, 14, 30, -1],
    ['pr0', 'pr0', 0, 14, 30, -1],
    ['pr1', 'pr1', 0, 14, 30, -1],
    ['pr2', 'pr2', 0, 14, 30, -1],
    ['pr3', 'pr3', 0, 14, 30, -1],
    ['bl0', 'bl0', 0, 14, 30, -1],
    ['bl1', 'bl1', 0, 14, 30, -1],
    ['bl2', 'bl2', 0, 14, 30, -1],
    ['bl3', 'bl3', 0, 14, 30, -1],
    ['plant', 'p1', 0, 24, 60, 1],
    ['rockeyes', 'r0', 0, 11, 60, 0],
    ['rockeyes', 'r1', 10, 6, 60, 0],
    ['rockeyes', 'r2', 15, 11, 60, 0],
    ['rockeyes', 'r3', 25, 6, 60, 0],
    ['rockeyes', 'r4', 30, 11, 60, 0],
    ['rockeyes', 'r5', 40, 6, 60, 0],
    ['rockeyes', 'r6', 45, 11, 60, 0],
    ['rockeyes', 'r7', 55, 6, 60, 0],
    ['rockeyes', 'r8', 60, 11, 60, 0],
    ['rockeyes', 'r9', 70, 6, 60, 0],
    ['rockl', 'rockl', 0, 12, 30, -1],
    ['absl', 'absl', 0, 12, 30, -1],
    ['farl', 'farl', 0, 12, 30, -1],
    ['mirl', 'mirl', 0, 12, 30, -1],
    ['poll', 'poll', 0, 12, 30, -1],
    ['mir', 'm3', 0, 15, 60, 0],
    ['mir', 'm2', 14, 15, 60, 0],
    ['mir', 'm1', 28, 15, 60, 0],
    ['mir', 'm0', 42, 15, 60, 0],
    ['spl', 'sp3', 0, 15, 60, 0],
    ['spl', 'sp2', 14, 15, 60, 0],
    ['spl', 'sp1', 28, 15, 60, 0],
    ['spl', 'sp0', 42, 15, 60, 0],
    ['ps', 'ps3', 0, 15, 60, 0],
    ['ps', 'ps2', 14, 15, 60, 0],
    ['ps', 'ps1', 28, 15, 60, 0],
    ['ps', 'ps0', 42, 15, 60, 0],
    ['expl', 'expl', 0, 15, 30, 0],
    ['count', 'count', 0, 12, 3, 0]
  ]

  for (let data of animData) {
    let frames = []
    let startFrame = data[2]
    let endFrame = startFrame + data[3]
    for (let j = startFrame; j < endFrame; j++) {
      let num = j + ''
      while (num.length < 4) {
        num = '0' + num
      }
      frames.push({
        key: ATLAS_1,
        frame: data[0] + '.' + num,
      })
    }

    sc.anims.create({
      key: data[1],
      frames: frames,
      frameRate: data[4],
      repeat: data[5],
    })
  }

  sc.anims.get('expl').hideOnComplete = true

}