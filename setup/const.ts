export const GAME_WIDTH: number = 1280
export const GAME_HEIGHT: number = 720

export const GOAL_WID: number = 100
export const GOAL_HGT: number = 8

export const START_ANGLES = [0, 45, 90, 135, 180, 225, 270, 315]

export const PI_14 = Math.PI * .25
export const PI_12 = Math.PI * .5
export const PI = Math.PI
export const PI2 = Math.PI * 2

export const STEM_COORDS: [number, number][] = [
  [-84, -7], // 0
  [-85, 23], // 1
  [-15, 40],   // 2
  [-14, 81], // 3
  [-2, 120], // 4
  [57, 67],  // 5
  [123, -16], // 6
  [30, -83], // 7
  [6, -124], // 8
  [15, -40],  // 9
]

export const CELL_SIZE: number = 80
export const HALF_CELL: number = 40

export const SEGMENT_DURATION: number = 100
export const HALF_SEGMENT_DURATION: number = 50


