import { POLARIZER, POLAR_SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level21: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 1, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [2, 4, 4, 1, 1, 0, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLAR_SPLITTER]: [
      [5, 1, 2],
      [8, 1, 2],
      [11, 1, 2],
      [5, 4, 2],
      [8, 4, 0],
      [11, 4, 2],
      [5, 7, 0],
      [8, 7, 2],
      [11, 7, 0]
    ],
    [POLARIZER]: [
      [7, 4, 0]
    ],
    [SUGAR]: [
      [4, 3, 0, 1],
      [6, 3, 0, 1],
      [7, 3, 0, 1],
      [9, 3, 0, 1],
      [10, 3, 0, 1],
      [4, 5, 0, 1],
      [6, 5, 0, 1],
      [7, 5, 0, 1],
      [9, 5, 0, 1],
      [10, 5, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level21