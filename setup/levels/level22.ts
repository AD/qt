import { GLASS, MIRROR, POLAR_SPLITTER, ROCK, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level22: ILevel = {

  // col, row, dir, pol(0,6)
  laserDef: [11, 4, 4, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [3, 7, 2, 1, 2, 7]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [1, 4, 0]
    ],
    [MIRROR]: [
      [3, 0, 3],
      [8, 0, 1],
      [5, 4, 1],
      [6, 4, 3],
      [5, 8, 1],
      [6, 8, 3]
    ],
    [SPLITTER]: [
      [3, 4, 3]
    ],
    [GLASS]: [
      [3, 2, 0]
    ],
    [POLAR_SPLITTER]: [
      [8, 4, 2]
    ],
    [SUGAR]: [
      [8, 6, 0, 1],
      [9, 6, 0, 1],
      [10, 6, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1]
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level22