import { ABSORBER, GLASS, MINE, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level14: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [7, 5, 2, .25, 2, 7],
    [9, 5, 2, .5, 0, 7, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MINE]: [
      [11, 3, 0]
    ],
    [ABSORBER]: [
      [3, 3, 0, 1]
    ],
    [SPLITTER]: [
      [5, 1, 1],
      [7, 3, 1],
      [9, 3, 1]
    ],
    [MIRROR]: [
      [9, 1, 1],
      [5, 3, 1]
    ],
    [GLASS]: [
      [6, 1, 0],
      [8, 1, 0]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1]
  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level14