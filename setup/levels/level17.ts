import { MIRROR, POLARIZER, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level17: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [12, 4, 0, .5, 1, 6, true],
    [12, 6, 0, .5, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLARIZER]: [
      [6, 1, 0],
      [6, 4, 2],
      [10, 4, 0],
      [10, 6, 2]
    ],
    [MIRROR]: [
      [8, 1, 1],
      [5, 4, 1],
      [8, 6, 1],
      [10, 1, 2, 1, 1]
    ],
    [SUGAR]: [
      [12, 1, 0, 1],
      [8, 4, 0, 1]
    ],
    [SPLITTER]: [
      [5, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level17