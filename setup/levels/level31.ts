import { FARADAY, MIRROR, POLAR_SPLITTER, ROCK } from '../strings';
import { ILevel } from "../types";

const level31: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [11, 5, 0, 1, 1, 1]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [4, 2, 0],
      [10, 2, 0]
    ],
    [POLAR_SPLITTER]: [
      [5, 1, 2],
      [7, 1, 0],
      [9, 1, 2],
      [5, 3, 0],
      [7, 3, 2],
      [9, 3, 0],
      [5, 5, 2],
      [7, 5, 0],
      [9, 5, 2]
    ],
    [FARADAY]: [
      [6, 2, 0, 1, 1],
      [8, 4, 0, 1, 1]
    ],
    [MIRROR]: [
      [8, 2, 0, 1, 1],
      [6, 4, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level31