import { GLASS, MINE, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level34: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [9, 1, 4, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [3, 7, 2, 1, 0, 7, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MINE]: [
      [1, 5, 0]
    ],
    [MIRROR]: [
      [5, 1, 3],
      [7, 3, 3],
      [3, 3, 3],
      [5, 5, 3]
    ],
    [SPLITTER]: [
      [7, 1, 3],
      [5, 3, 3],
      [3, 5, 3]
    ],
    [GLASS]: [
      [3, 1, 0, 1],
      [7, 5, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1],
    [-1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1]
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level34