import { MIRROR, ROCK } from '../strings';
import { ILevel } from "../types";

const level02: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [4, 2, 2, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [6, 2, 6, 1, 0, 4]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [2, 4, 0],
      [10, 6, 0],
    ],
    [MIRROR]: [
      [4, 4, 3, 0, 1],
      [8, 4, 1],
      [6, 6, 1],
      [8, 6, 1, 0, 1],
    ],
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level02