import { GLASS, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level11: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 2, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [5, 0, 6, 1, 0, 2]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [5, 2, 1]
    ],
    [GLASS]: [
      [8, 2, 0],
      [5, 3, 0],
      [5, 5, 0],
      [5, 7, 0]
    ],
    [MIRROR]: [
      [3, 4, 1, 1, 1],
      [7, 4, 1, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level11