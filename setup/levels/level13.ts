import { ABSORBER, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level13: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 2, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [9, 2, 0, .25, 1, 5, true],
    [5, 6, 2, .25, 0, 7, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [5, 2, 1]
    ],
    [ABSORBER]: [
      [7, 4, 0, 1],
      [9, 6, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level13