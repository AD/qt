import { MIRROR, POLARIZER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level19: ILevel = {

  // col, row, ang, pol(0,6)
  laserDef: [6, 6, 6, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [10, 5, 0, 1, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SUGAR]: [
      [3, 3, 0, 1],
      [4, 4, 0, 1],
      [5, 3, 0, 1],
      [7, 3, 0, 1],
      [8, 4, 0, 1]
    ],
    [POLARIZER]: [
      [6, 2, 0],
      [1, 4, 2],
      [8, 5, 2]
    ],
    [MIRROR]: [
      [4, 1, 3],
      [6, 1, 1],
      [1, 2, 3],
      [4, 2, 3],
      [1, 5, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, 0, 0, 0, 0, 0, -1, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1],

  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level19