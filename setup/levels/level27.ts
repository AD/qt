import { MINE, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level27: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [10, 7, 2, 1, 0, 7]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MINE]: [
      [12, 5, 0],
      [8, 7, 0]
    ],
    [MIRROR]: [
      [8, 1, 1],
      [10, 3, 1],
      [4, 5, 1]
    ],
    [SPLITTER]: [
      [4, 1, 1],
      [6, 1, 0, 1, 1],
      [4, 3, 2, 1, 1],
      [8, 3, 2, 1, 1],
      [6, 5, 0, 1, 1],
      [8, 5, 3, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_c0'],
  backTex: 'back_c0'

}

export default level27