import { MIRROR, POLARIZER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level16: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [2, 3, 4, 1, 1, 6]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLARIZER]: [
      [5, 1, 3],
      [8, 1, 1],
      [11, 1, 0],
      [8, 3, 2],
      [5, 3, 1],
    ],
    [MIRROR]: [
      [14, 1, 1],
      [14, 3, 3]
    ],
    [SUGAR]: [
      [1, 2, 0, 1],
      [3, 2, 0, 1],
      [5, 2, 0, 1],
      [7, 2, 0, 1],
      [9, 2, 0, 1],
      [11, 2, 0, 1],
      [13, 2, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level16