import { MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level10: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 4, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 6, 2, 1, 0, 7, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [8, 4, 3]
    ],
    [MIRROR]: [
      [4, 3, 0, 1, 1],
      [4, 5, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
  ],

  gridFrames: ['grid_c0'],
  backTex: 'back_c0'

}

export default level10