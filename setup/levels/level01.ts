import { ROCK } from '../strings';
import { ILevel } from "../types";

const level01: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 3, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 3, 0, 1, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [5, 3, 0, 1, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level01