import { GLASS, MIRROR, ROCK, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level07: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 5, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 1, 6, 1, 0, 3, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [10, 3, 0, 0, 0]
    ],
    [SPLITTER]: [
      [8, 3, 3, 0, 0],
      [5, 5, 3, 0, 0]
    ],
    [MIRROR]: [
      [5, 3, 3, 0, 0],
      [8, 5, 3, 0, 0]
    ],
    [GLASS]: [
      [5, 1, 0, 1, 0],
      [10, 5, 0, 1, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level07