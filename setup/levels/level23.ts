import { MINE, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level23: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [10, 3, 0, .5, 1, 1],
    [10, 5, 0, .5, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MINE]: [
      [10, 1, 0],
      [4, 7, 0],
      [6, 7, 0],
      [8, 7, 0]
    ],
    [SPLITTER]: [
      [4, 1, 1],
      [5, 2, 0, 1, 1],
      [7, 2, 2, 1, 1],
      [5, 6, 2, 1, 1],
      [7, 6, 0, 1, 1]
    ],
    [MIRROR]: [
      [3, 2, 2, 1, 1],
      [2, 6, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level23