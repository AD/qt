import { MINE, MIRROR, ROCK, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level12: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 7, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [7, 1, 6, .25, 2, 3],
    [11, 3, 0, .5, 1, 1],
    [11, 5, 0, .25, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [7, 5, 0]
    ],
    [MIRROR]: [
      [5, 3, 3],
      [9, 7, 3]
    ],
    [SPLITTER]: [
      [3, 5, 0, 1, 1],
      [5, 5, 0, 1, 1],
      [7, 7, 0, 1, 1],
      [5, 7, 3],
    ],
    [MINE]: [
      [9, 1, 0]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level12