import { POLARIZER } from '../strings';
import { ILevel } from "../types";

const level18: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [5, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [13, 1, 0, .25, 1, 1]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLARIZER]: [
      [7, 1, 0],
      [11, 1, 2],
      [1, 1, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level18