import { ABSORBER, FARADAY, GLASS, MIRROR, POLARIZER, POLAR_SPLITTER, ROCK, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level36: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [11, 3, 0, .5, 1, 1],
    [5, 5, 4, .25, 1, 6]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLARIZER]: [
      [5, 1, 1]
    ],
    [SPLITTER]: [
      [6, 1, 1],
      [7, 3, 1],
      [9, 3, 1]
    ],
    [POLAR_SPLITTER]: [
      [8, 1, 0, 1, 1]
    ],
    [FARADAY]: [
      [10, 1, 0, 1, 1]
    ],
    [MIRROR]: [
      [3, 3, 0, 1, 1],
      [8, 4, 0, 1, 1]
    ],
    [GLASS]: [
      [5, 3, 0, 1],
      [11, 5, 0, 1]
    ],
    [ABSORBER]: [
      [9, 5, 0, 1]
    ],
    [SUGAR]: [
      [7, 5, 0, 1]
    ],
    [ROCK]: [
      [6, 4, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_a0'],
  backTex: 'back_a0'

}

export default level36