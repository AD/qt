import { FARADAY, MIRROR, POLAR_SPLITTER, ROCK, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level30: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 3, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 1, 6, 1, 0, 3]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [6, 1, 0],
      [6, 5, 0]
    ],
    [POLAR_SPLITTER]: [
      [6, 3, 0]
    ],
    [SPLITTER]: [
      [8, 3, 1]
    ],
    [MIRROR]: [
      [11, 3, 1],
      [11, 6, 3],
      [8, 6, 1]
    ],
    [SUGAR]: [
      [7, 3, 0],
      [2, 1, 0, 1],
      [4, 1, 0, 1]
    ],
    [FARADAY]: [
      [2, 5, 0, 1, 1],
      [4, 5, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level30