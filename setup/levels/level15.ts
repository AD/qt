import { MIRROR, SUGAR } from '../strings';
import { ILevel } from "../types";

const level15: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [2, 5, 4, 1, 1, 1, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SUGAR]: [
      [5, 1, 0],
      [8, 1, 0],
      [11, 2, 0],
      [11, 3, 0],
      [8, 5, 0],
      [9, 5, 0]
    ],
    [MIRROR]: [
      [4, 5, 0, 1, 1],
      [6, 5, 2, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_c0'],
  backTex: 'back_c0'

}

export default level15