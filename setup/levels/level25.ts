import { GLASS, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level25: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [5, 6, 6, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [1, 4, 4, 1, 1, 5]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [7, 5, 3, 1, 1],
      [5, 4, 3]
    ],
    [MIRROR]: [
      [5, 1, 3],
      [7, 1, 1],
      [7, 3, 3],
      [3, 1, 3, 1, 1]
    ],
    [GLASS]: [
      [3, 3, 0, 1],
      [9, 1, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1, -1, -1]
  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level25