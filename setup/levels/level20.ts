import { POLAR_SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level20: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [6, 0, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 4, 0, .25, 1, 0],
    [6, 6, 2, .25, 0, 7, true],
    [2, 6, 4, .25, 1, 0, true],
    [4, 8, 2, .25, 0, 9]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLAR_SPLITTER]: [
      [11, 0, 2],
      [11, 2, 0],
      [6, 2, 0],
      [5, 2, 2],
      [1, 2, 0],
      [1, 4, 2],
      [4, 4, 2],
      [4, 6, 0],
      [7, 2, 0, 1, 1]
    ],
    [SUGAR]: [
      [2, 2, 0],
      [3, 2, 0],
      [8, 2, 0],
      [9, 2, 0],
      [6, 4, 0, 1],
      [3, 5, 0, 1],
      [5, 4, 0, 1],
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1],
    [-1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1]
  ],

  gridFrames: ['grid_c0'],
  backTex: 'back_c0'

}

export default level20