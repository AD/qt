import { MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level06: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 3, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [12, 3, 0, 1, 1, 0]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [4, 3, 1],
      [6, 5, 1],
      [10, 3, 3],
      [8, 5, 3]
    ],
    [MIRROR]: [
      [4, 5, 1],
      [6, 3, 1],
      [7, 1, 0, 1, 1],
      [7, 7, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1]
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level06