import { MIRROR, ROCK } from '../strings';
import { ILevel } from "../types";

const level03: ILevel = {


  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 2, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [8, 2, 6, 1, 0, 4, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [5, 2, 0]
    ],
    [MIRROR]: [
      [2, 4, 0, 1, 1],
      [8, 4, 0, 1, 1],
      [4, 5, 1, 0, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1]
  ],

  gridFrames: ['grid_c0', 'grid_c0'],
  backTex: 'back_c0'

}

export default level03



