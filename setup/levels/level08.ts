import { GLASS, MIRROR, SPLITTER } from '../strings';
import { ILevel } from "../types";

const level08: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 1, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [12, 1, 0, .5, 1, 0],
    [10, 7, 0, .5, 1, 1]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [4, 1, 1],
      [6, 3, 1],
      [6, 5, 1],
      [8, 7, 1],
      [8, 3, 3],
      [10, 1, 3]
    ],
    [MIRROR]: [
      [6, 1, 1],
      [4, 3, 1],
      [8, 5, 1],
      [6, 7, 1],
      [8, 1, 3],
      [10, 3, 3]
    ],
    [GLASS]: [
      [4, 5, 0, 1],
      [10, 5, 0, 1],
      [4, 7, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1]
  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level08