import { POLAR_SPLITTER, ROCK, SUGAR } from '../strings';
import { ILevel } from "../types";

const level20a: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [2, 2, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [6, 0, 6, 1, 0, 2]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [POLAR_SPLITTER]: [
      [6, 2, 2, 0, 1]
    ],
    [SUGAR]: [
      [4, 3, 0, 1],
      [8, 3, 0, 1]
    ],
    [ROCK]: [
      [10, 2, 0],
      [6, 4, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1],

  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level20a