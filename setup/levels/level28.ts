import { FARADAY, MIRROR, POLAR_SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level28: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 2, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [7, 5, 2, 1, 0, 7, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SUGAR]: [
      [9, 2, 0, 1],
      [11, 2, 0, 1]
    ],
    [POLAR_SPLITTER]: [
      [7, 2, 0]
    ],
    [MIRROR]: [
      [13, 2, 2]
    ],
    [FARADAY]: [
      [7, 1, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1],
    [-1, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1, -1]
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level28