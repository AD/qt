import { MIRROR, POLAR_SPLITTER, ROCK, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level35: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 1, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [11, 5, 2, .5, 0, 8],
    [9, 7, 2, .5, 2, 7]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [ROCK]: [
      [13, 3, 0],
      [7, 5, 0]
    ],
    [SUGAR]: [
      [5, 1, 0, 1],
      [5, 5, 0, 1]
    ],
    [SPLITTER]: [
      [7, 3, 1, 1, 1],
      [9, 3, 1]
    ],
    [MIRROR]: [
      [9, 1, 1],
      [5, 3, 1]
    ],
    [POLAR_SPLITTER]: [
      [11, 3, 2],
      [9, 5, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1, -1, -1],
  ],

  gridFrames: ['grid_e0'],
  backTex: 'back_e0'

}

export default level35