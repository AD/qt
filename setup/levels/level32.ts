import { FARADAY, MIRROR, POLAR_SPLITTER, SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level32: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 7, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [10, 4, 0, 1, 1, 1]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MIRROR]: [
      [4, 1, 3],
      [8, 1, 1],
      [4, 6, 1],
      [6, 5, 3],
      [8, 5, 3],
      [6, 7, 3]
    ],
    [SPLITTER]: [
      [6, 1, 2],
      [6, 6, 1]
    ],
    [POLAR_SPLITTER]: [
      [5, 3, 0, 1, 1]
    ],
    [SUGAR]: [
      [6, 3, 0, 1]
    ],
    [FARADAY]: [
      [7, 3, 0, 1, 1]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ],

  gridFrames: ['grid_b0'],
  backTex: 'back_b0'

}

export default level32