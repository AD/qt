import { FARADAY, MINE, MIRROR, POLAR_SPLITTER, SUGAR } from '../strings';
import { ILevel } from "../types";

const level33: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 3, 0, 6],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [3, 1, 4, 1, 1, 1, true]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [MINE]: [
      [3, 5, 0]
    ],
    [MIRROR]: [
      [13, 3, 2],
      [13, 5, 2],
      [3, 7, 2]
    ],
    [POLAR_SPLITTER]: [
      [6, 1, 0],
      [9, 1, 0],
      [12, 1, 2],
      [6, 3, 2],
      [8, 3, 0],
      [11, 3, 0],
      [6, 5, 0],
      [9, 5, 2],
      [11, 5, 0],
      [6, 7, 0],
      [8, 7, 2],
      [9, 7, 0],
      [12, 7, 0]
    ],
    [FARADAY]: [
      [5, 2, 0, 1, 1],
      [7, 2, 0, 1, 1],
      [10, 2, 0, 1, 1],
      [5, 4, 0, 1, 1],
      [5, 6, 0, 1, 1]
    ],
    [SUGAR]: [
      [7, 4, 0, 1],
      [10, 4, 0, 1],
      [7, 6, 0, 1],
      [10, 6, 0, 1]
    ]
  },

  // show tracers or not
  showTrace: false,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1]
  ],

  gridFrames: ['grid_c0'],
  backTex: 'back_c0'

}

export default level33