import { SPLITTER } from '../strings';
import { ILevel } from "../types";

const level04: ILevel = {

  // col, row, dir/ang, pol(0,6)
  laserDef: [3, 5, 0, 0],

  // col, row, ang, goal, grad-dir (0,1,2)
  detectorDef: [
    [2, 3, 4, .25, 1, 5],
    [6, 1, 6, .25, 0, 3],
    [10, 5, 0, .25, 1, 1],
    [8, 7, 2, .25, 0, 7]
  ],

  // game pieces
  gamePieces: {
    // col, row, ang, ?moves (0,1), ?rotates (0,1)
    [SPLITTER]: [
      [4, 3, 2, 1, 1],
      [4, 7, 2, 1, 1],
      [6, 5, 3, 0, 0]
    ]
  },

  // show tracers or not
  showTrace: true,

  // -1 for blank, else frame index 
  gridDef: [
    [-1, -1, -1, 0, 0, 0, 0, 0, 0, -1, -1, -1],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1]
  ],

  gridFrames: ['grid_d0'],
  backTex: 'back_d0'

}

export default level04



