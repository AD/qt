import { ATLAS_1 } from "./strings"

export default function (sc: Phaser.Scene, frames: string[]) {

  // for texture frames that need to tile cleanly, 
  // reduce frame by one pixel from all sides

  for (let frame of frames) {
    let fr = sc.textures.getFrame(ATLAS_1, frame)
    let newCutx = fr.cutX + 1
    let newCuty = fr.cutY + 1
    let newWidth = fr.cutWidth - 2
    let newHeight = fr.cutHeight - 2
    let atlasKey = sc.textures.get(ATLAS_1)
    atlasKey.remove(frame)
    atlasKey.add(frame, 0, newCutx, newCuty, newWidth, newHeight)
  }

} 