// *********************
//   LEVEL DEFINITIONS
// *********************

import { ILevel } from './types'
import level01 from './levels/level01'
import level02 from './levels/level02'
import level03 from './levels/level03'
import level04 from './levels/level04'
import level06 from './levels/level06'
import level07 from './levels/level07'
import level08 from './levels/level08'
import level10 from './levels/level10'
import level11 from './levels/level11'
import level12 from './levels/level12'
import level13 from './levels/level13'
import level14 from './levels/level14'
import level15 from './levels/level15'
import level16 from './levels/level16'
import level17 from './levels/level17'
import level18 from './levels/level18'
import level19 from './levels/level19'
import level20 from './levels/level20'
import level20a from './levels/level20a'
import level21 from './levels/level21'
import level22 from './levels/level22'
import level23 from './levels/level23'
import level25 from './levels/level25'
import level27 from './levels/level27'
import level28 from './levels/level28'
import level30 from './levels/level30'
import level31 from './levels/level31'
import level32 from './levels/level32'
import level33 from './levels/level33'
import level34 from './levels/level34'
import level35 from './levels/level35'
import level36 from './levels/level36'

const levels: ILevel[] = []

// adding blank for level 0 
let level0: ILevel = {
  // name: "",
  laserDef: [0, 0, 0, 0],
  detectorDef: [[0, 0, 0, 0, 0, 0]],
  gamePieces: {},
  showTrace: false,
  gridDef: [
    []
  ],
  gridFrames: [],
  backTex: ""
}
levels.push(level0)

// put the levels in order here  
levels.push(level01)
levels.push(level02)
levels.push(level03)
levels.push(level04)
levels.push(level06)
levels.push(level07)
levels.push(level08)
levels.push(level10)
levels.push(level11)
levels.push(level12)
levels.push(level13)
levels.push(level14)
levels.push(level15)
levels.push(level16)
levels.push(level17)
levels.push(level18)
levels.push(level19)
levels.push(level20a)
levels.push(level20)
levels.push(level21)
levels.push(level22)
levels.push(level23)
levels.push(level25)
levels.push(level27)
levels.push(level28)
levels.push(level30)
levels.push(level31)
levels.push(level32)
levels.push(level34)
levels.push(level35)
levels.push(level36)
levels.push(level33)

export default levels

