export interface ILevel {
  laserDef: [number, number, number, number]
  // col, row, ang, goal, goal-dir, stem, stemFlip 
  detectorDef: [number, number, number, number, number, number, boolean?][]
  gamePieces: {
    [k: string]: number[][]
  }
  showTrace: boolean
  gridDef: number[][]
  gridFrames: string[]
  backTex: string
}

export interface INext {
  per: number
  ha: number
  va: number
  hp: number
  vp: number
  dir: number
  col: number
  row: number
  id: number
}

export interface IPath {
  startT: number
  startX: number
  startY: number
  param: string
  dur: number
  endV: number
  per: number
  endFn?: () => void
}

export interface IStep {
  pathID: string
  startX: number
  startY: number
  dir: number
  param: string
  delta: number
  ha: number
  va: number
  hp: number
  vp: number
}