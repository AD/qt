
export default function (sc: Phaser.Scene, obj: Phaser.GameObjects.Image) {
  return sc.tweens.add({
    targets: obj,
    duration: 150,
    ease: 'Cubic.easeOut',
    yoyo: true,
    repeat: 30,
    props: {
      y: obj.y - 10
    },
    paused: true
  })
}