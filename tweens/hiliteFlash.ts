import GameScene from '../scenes/GameScene'

export default function (sc: GameScene) {
  sc.tweens.killTweensOf(sc.hilite)
  sc.hilite.alpha = 0.3
  sc.tweens.add({
    targets: sc.hilite,
    duration: 1000,
    ease: 'Cubic.easeOut',
    alpha: 0,
  })
}
