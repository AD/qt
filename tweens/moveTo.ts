import GamePiece from '../game pieces/GamePiece'
import { TRACE } from '../setup/strings'

export default function (sc: Phaser.Scene, obj: GamePiece, endX: number, endY: number) {
  let dist = Phaser.Math.Distance.Between(obj.x, obj.y, endX, endY)
  let dur = Math.sqrt(dist) * 20

  sc.tweens.add({
    targets: obj,
    duration: dur,
    ease: 'Cubic.easeOut',
    props: {
      x: endX,
      y: endY
    },
    onComplete: () => {
      obj.showIcons()
      sc.events.emit(TRACE)
    }
  })

}
