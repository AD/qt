import GamePiece from '../game pieces/GamePiece'
import { TRACE } from '../setup/strings'

export default function (sc: Phaser.Scene, obj: GamePiece, diff: string) {

  sc.tweens.add({
    targets: obj,
    duration: 250,
    ease: 'Cubic.easeInOut',
    angle: diff,
    onComplete: () => {
      obj.angle = obj.ang * 45
      obj.showIcons()
      sc.events.emit(TRACE)
    },
  })
}