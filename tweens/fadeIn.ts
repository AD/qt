
export default function (sc: Phaser.Scene, obj: Phaser.GameObjects.GameObject, delay?: number) {

  let _delay = delay || 0

  sc.tweens.add({
    targets: obj,
    duration: 450,
    ease: 'Quad.easeOut',
    alpha: 1,
    delay: _delay
  })
}
