
export default function (sc: Phaser.Scene, obj: Phaser.GameObjects.GameObject, dur: number, delay?: number) {

  let _delay = delay || 0

  sc.tweens.add({
    targets: obj,
    duration: dur,
    ease: 'Quad.easeIn',
    alpha: 0,
    delay: _delay
  })
}