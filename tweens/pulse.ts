
export default function (sc: Phaser.Scene, obj: Phaser.GameObjects.Image) {
  return sc.tweens.add({
    targets: obj,
    duration: 150,
    ease: 'Cubic.easeInOut',
    yoyo: true,
    repeat: 30,
    alpha: 1,
    paused: true
  })
}